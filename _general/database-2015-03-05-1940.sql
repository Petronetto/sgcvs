-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sgcvs
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sgcvs
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sgcvs` DEFAULT CHARACTER SET utf8 ;
USE `sgcvs` ;

-- -----------------------------------------------------
-- Table `sgcvs`.`agencies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`agencies` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`agencies` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `responsible` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `telephone` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `cellphone` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`employers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`employers` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`employers` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `cpf` VARCHAR(11) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `rg` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `cellphone` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`rdas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`rdas` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`rdas` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `email` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `type` TINYINT(1) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`ports`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`ports` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`ports` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `stevedore` TINYINT(1) NULL DEFAULT NULL,
  `boat` TINYINT(1) NULL DEFAULT NULL,
  `state_id` INT(11) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`states`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`states` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`states` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `abbr` CHAR(2) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`groups` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`groups` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`localities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`localities` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`localities` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`users` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `username` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `email` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `password` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `remember_token` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `locale_id` INT(10) UNSIGNED NOT NULL,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  INDEX `fk_group_id` (`group_id` ASC),
  INDEX `fk_locale_id` (`locale_id` ASC),
  CONSTRAINT `fk_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `sgcvs`.`groups` (`id`),
  CONSTRAINT `fk_locale_id`
    FOREIGN KEY (`locale_id`)
    REFERENCES `sgcvs`.`localities` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`vehicles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`vehicles` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`vehicles` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plaque` VARCHAR(8) CHARACTER SET 'utf8' NOT NULL,
  `model` VARCHAR(255) CHARACTER SET 'utf8' NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`vessels`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`vessels` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`vessels` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `email` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`feeds`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`feeds` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`feeds` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `vessel_id` INT(10) UNSIGNED NOT NULL,
  `rda_id` INT(10) UNSIGNED NOT NULL,
  `corda_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `agency_id` INT(10) UNSIGNED NOT NULL,
  `port_id` INT(10) UNSIGNED NOT NULL,
  `employer1_id` INT(10) UNSIGNED NOT NULL,
  `employer2_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `employer3_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `vehicle_id` INT(10) UNSIGNED NOT NULL,
  `state_id` INT(10) UNSIGNED NOT NULL,
  `status` TINYINT(1) NOT NULL,
  `invoice` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `nfe` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `groups` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `eta` DATE NOT NULL,
  `etb` DATE NULL DEFAULT NULL,
  `ets` DATE NULL DEFAULT NULL,
  `payment` INT(10) UNSIGNED NOT NULL,
  `lawyer` TINYINT(1) NOT NULL DEFAULT '0',
  `under_process` TINYINT(1) NOT NULL DEFAULT '0',
  `obs` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  INDEX `fk_agency` (`agency_id` ASC),
  INDEX `fk_state_id` (`state_id` ASC),
  INDEX `fk_user_id` (`user_id` ASC),
  INDEX `fk_vehicle_id` (`vehicle_id` ASC),
  INDEX `fk_vessel_id` (`vessel_id` ASC),
  INDEX `fk_port_id_idx` (`port_id` ASC),
  INDEX `fk_rda_id_idx` (`rda_id` ASC),
  CONSTRAINT `fk_agency`
    FOREIGN KEY (`agency_id`)
    REFERENCES `sgcvs`.`agencies` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_corda_id`
    FOREIGN KEY (`corda_id`)
    REFERENCES `sgcvs`.`rdas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_employer1_id`
    FOREIGN KEY (`employer1_id`)
    REFERENCES `sgcvs`.`employers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_employer2_id`
    FOREIGN KEY (`employer2_id`)
    REFERENCES `sgcvs`.`employers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_employer3_id`
    FOREIGN KEY (`employer3_id`)
    REFERENCES `sgcvs`.`employers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_port_id`
    FOREIGN KEY (`port_id`)
    REFERENCES `sgcvs`.`ports` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rda_id`
    FOREIGN KEY (`rda_id`)
    REFERENCES `sgcvs`.`rdas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_state_id`
    FOREIGN KEY (`state_id`)
    REFERENCES `sgcvs`.`states` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `sgcvs`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `sgcvs`.`vehicles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vessel_id`
    FOREIGN KEY (`vessel_id`)
    REFERENCES `sgcvs`.`vessels` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`password_resets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`password_resets` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`password_resets` (
  `email` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `token` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX `password_resets_email_index` (`email` ASC),
  INDEX `password_resets_token_index` (`token` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`permissions` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`permissions` (
  `group_id` INT(10) UNSIGNED NOT NULL,
  `operational` TINYINT(1) NULL DEFAULT '1',
  `line_up` TINYINT(1) NULL DEFAULT '0',
  `approvals` TINYINT(1) NULL DEFAULT '0',
  `billing` TINYINT(1) NULL DEFAULT '0',
  `settlement` TINYINT(1) NULL DEFAULT '0',
  `historic` TINYINT(1) NULL DEFAULT '0',
  `vessel` TINYINT(1) NULL DEFAULT '0',
  `rda` TINYINT(1) NULL DEFAULT '0',
  `agency` TINYINT(1) NULL DEFAULT '0',
  `port` TINYINT(1) NULL DEFAULT '0',
  `employer` TINYINT(1) NULL DEFAULT '0',
  `vehicles` TINYINT(1) NULL DEFAULT '0',
  `users` TINYINT(1) NULL DEFAULT '0',
  `reports` TINYINT(1) NULL DEFAULT '0',
  INDEX `fk_group_id_permissions` (`group_id` ASC),
  CONSTRAINT `fk_group_id_permissions`
    FOREIGN KEY (`group_id`)
    REFERENCES `sgcvs`.`groups` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`remarks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`remarks` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`remarks` (
  `feed_id` INT(10) UNSIGNED NOT NULL,
  `nautical_chart` TINYINT(1) NOT NULL DEFAULT '0',
  `join_request` TINYINT(1) NOT NULL DEFAULT '0',
  `feedback` TINYINT(1) NOT NULL DEFAULT '0',
  `medicines` TINYINT(1) NOT NULL DEFAULT '0',
  `boat_vs` TINYINT(1) NOT NULL DEFAULT '0',
  `estimate_vs` TINYINT(1) NOT NULL DEFAULT '0',
  `boat_client` TINYINT(1) NOT NULL DEFAULT '0',
  `estimate_client` TINYINT(1) NOT NULL DEFAULT '0',
  `others` TEXT NULL,
  INDEX `fk_feed_id` (`feed_id` ASC),
  CONSTRAINT `fk_feed_id`
    FOREIGN KEY (`feed_id`)
    REFERENCES `sgcvs`.`feeds` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`values` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`values` (
  `feed_id` INT(10) UNSIGNED NOT NULL,
  `gross_1` FLOAT NULL DEFAULT NULL,
  `discount_1` FLOAT NULL DEFAULT NULL,
  `gross_2` FLOAT NULL DEFAULT NULL,
  `discount_2` FLOAT NULL DEFAULT NULL,
  `wo` FLOAT NULL DEFAULT NULL,
  `boat` FLOAT NULL DEFAULT NULL,
  `transport` FLOAT NULL DEFAULT NULL,
  `received` FLOAT NULL DEFAULT NULL,
  `reference` FLOAT NULL DEFAULT NULL,
  `1_charge` DATE NULL DEFAULT NULL,
  `2_chage` DATE NULL DEFAULT NULL,
  `3_charge` DATE NULL DEFAULT NULL,
  `payment_date` DATE NULL DEFAULT NULL,
  `payment_remarks` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `purch_value_nfe` FLOAT NULL DEFAULT NULL,
  `boat_cost` FLOAT NULL DEFAULT NULL,
  `stevedore_cost` DOUBLE NULL DEFAULT NULL,
  `freight` DOUBLE NULL DEFAULT NULL,
  INDEX `fk_feeds_id` (`feed_id` ASC),
  CONSTRAINT `fk_feeds_id`
    FOREIGN KEY (`feed_id`)
    REFERENCES `sgcvs`.`feeds` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
