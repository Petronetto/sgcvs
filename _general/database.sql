-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema sgcvs
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sgcvs
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sgcvs` DEFAULT CHARACTER SET utf8 ;
USE `sgcvs` ;

-- -----------------------------------------------------
-- Table `sgcvs`.`agencies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`agencies` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`agencies` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `responsible` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `telephone` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `cellphone` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`employers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`employers` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`employers` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `cpf` VARCHAR(11) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `rg` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `cellphone` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`ports`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`ports` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`ports` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `stevedore` TINYINT(1) NULL DEFAULT NULL,
  `boat` TINYINT(1) NULL DEFAULT NULL,
  `state_id` INT(11) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`rdas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`rdas` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`rdas` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `email` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `type` TINYINT(1) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`states`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`states` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`states` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(15) CHARACTER SET 'utf8' NOT NULL,
  `abbreviation` CHAR(2) CHARACTER SET 'utf8' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`groups` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`localities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`localities` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`localities` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE UNIQUE INDEX `name_UNIQUE` ON `sgcvs`.`localities` (`name` ASC);


-- -----------------------------------------------------
-- Table `sgcvs`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`users` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `username` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL DEFAULT '',
  `email` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL DEFAULT '',
  `password` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL DEFAULT '',
  `remember_token` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `locale_id` INT(11) NOT NULL,
  `group_id` INT(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `sgcvs`.`groups` (`id`),
  CONSTRAINT `locale_id`
    FOREIGN KEY (`locale_id`)
    REFERENCES `sgcvs`.`localities` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE INDEX `locale_id_idx` ON `sgcvs`.`users` (`locale_id` ASC);

CREATE INDEX `group_id_idx` ON `sgcvs`.`users` (`group_id` ASC);


-- -----------------------------------------------------
-- Table `sgcvs`.`vehicles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`vehicles` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`vehicles` (
  `id` INT(11) NOT NULL,
  `plaque` VARCHAR(8) CHARACTER SET 'utf8' NOT NULL,
  `model` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`vessels`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`vessels` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`vessels` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `email` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`feeds`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`feeds` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`feeds` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `vessel_id` INT(11) NOT NULL,
  `rda_id` INT(11) NOT NULL,
  `agency_id` INT(11) NOT NULL,
  `port_id` INT(11) NOT NULL,
  `employer1_id` INT(11) NULL,
  `employer2_id` INT(11) NULL,
  `employer3_id` INT(11) NOT NULL,
  `vehicle_id` INT(11) NOT NULL,
  `state_id` INT(11) NOT NULL,
  `status` TINYINT(1) NOT NULL,
  `invoice` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `nfe` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `groups` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `eta` DATE NOT NULL,
  `etb` DATE NULL,
  `ets` DATE NULL,
  `corda_id` INT(11) NULL,
  `payment` INT(10) UNSIGNED NOT NULL,
  `lawyer` TINYINT(1) NOT NULL DEFAULT '0',
  `under_process` TINYINT(1) NOT NULL DEFAULT '0',
  `obs` TEXT CHARACTER SET 'utf8' NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_agency`
    FOREIGN KEY (`agency_id`)
    REFERENCES `sgcvs`.`agencies` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_employer1_id`
    FOREIGN KEY (`employer1_id`)
    REFERENCES `sgcvs`.`employers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_employer2_id`
    FOREIGN KEY (`employer2_id`)
    REFERENCES `sgcvs`.`employers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_employer3_id`
    FOREIGN KEY (`employer3_id`)
    REFERENCES `sgcvs`.`employers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_port_id`
    FOREIGN KEY (`port_id`)
    REFERENCES `sgcvs`.`ports` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rda_id`
    FOREIGN KEY (`rda_id`)
    REFERENCES `sgcvs`.`rdas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_state_id`
    FOREIGN KEY (`state_id`)
    REFERENCES `sgcvs`.`states` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `sgcvs`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `sgcvs`.`vehicles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vessel_id`
    FOREIGN KEY (`vessel_id`)
    REFERENCES `sgcvs`.`vessels` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_corda_id`
    FOREIGN KEY (`corda_id`)
    REFERENCES `sgcvs`.`rdas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE INDEX `fk_user_id_idx` ON `sgcvs`.`feeds` (`user_id` ASC);

CREATE INDEX `fk_vessel_id_idx` ON `sgcvs`.`feeds` (`vessel_id` ASC);

CREATE INDEX `fk_rda_id_idx` ON `sgcvs`.`feeds` (`rda_id` ASC);

CREATE INDEX `fk_agency_idx` ON `sgcvs`.`feeds` (`agency_id` ASC);

CREATE INDEX `fk_port_id_idx` ON `sgcvs`.`feeds` (`port_id` ASC);

CREATE INDEX `fk_employer1_id_idx` ON `sgcvs`.`feeds` (`employer1_id` ASC);

CREATE INDEX `fk_employer2_id_idx` ON `sgcvs`.`feeds` (`employer2_id` ASC);

CREATE INDEX `fk_employer3_id_idx` ON `sgcvs`.`feeds` (`employer3_id` ASC);

CREATE INDEX `fk_vehicle_id_idx` ON `sgcvs`.`feeds` (`vehicle_id` ASC);

CREATE INDEX `fk_state_id_idx` ON `sgcvs`.`feeds` (`state_id` ASC);

CREATE INDEX `fk_corda_id_idx` ON `sgcvs`.`feeds` (`corda_id` ASC);


-- -----------------------------------------------------
-- Table `sgcvs`.`values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`values` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`values` (
  `id` INT(11) NOT NULL,
  `gross_1` DOUBLE NULL DEFAULT NULL,
  `discount_1` INT(11) NULL DEFAULT NULL,
  `gross_2` DOUBLE NULL DEFAULT NULL,
  `discount_2` INT(11) NULL DEFAULT NULL,
  `wo` INT(11) NULL DEFAULT NULL,
  `boat` INT(11) NULL DEFAULT NULL,
  `transport` INT(11) NULL DEFAULT NULL,
  `received` INT(11) NULL DEFAULT NULL,
  `reference` INT(11) NULL DEFAULT NULL,
  `1_charge` DATE NULL DEFAULT NULL,
  `2_chage` DATE NULL DEFAULT NULL,
  `3_charge` DATE NULL DEFAULT NULL,
  `payment_date` DATE NULL DEFAULT NULL,
  `payment_remarks` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_feeds_id`
    FOREIGN KEY (`id`)
    REFERENCES `sgcvs`.`feeds` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`cost_values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`cost_values` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`cost_values` (
  `id` INT(11) NOT NULL,
  `purch_value_nfe` DOUBLE NULL DEFAULT NULL,
  `boat` DOUBLE NULL DEFAULT NULL,
  `stevedore` DOUBLE NULL DEFAULT NULL,
  `freight` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_cost_values_id`
    FOREIGN KEY (`id`)
    REFERENCES `sgcvs`.`values` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`migrations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`migrations` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`migrations` (
  `migration` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `batch` INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`password_resets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`password_resets` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`password_resets` (
  `email` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `token` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE INDEX `password_resets_email_index` ON `sgcvs`.`password_resets` (`email` ASC);

CREATE INDEX `password_resets_token_index` ON `sgcvs`.`password_resets` (`token` ASC);


-- -----------------------------------------------------
-- Table `sgcvs`.`permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`permissions` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`permissions` (
  `group_id` INT(11) NOT NULL,
  `operational` TINYINT(1) NULL DEFAULT '1',
  `line_up` TINYINT(1) NULL DEFAULT '0',
  `approvals` TINYINT(1) NULL DEFAULT '0',
  `billing` TINYINT(1) NULL DEFAULT '0',
  `settlement` TINYINT(1) NULL DEFAULT '0',
  `historic` TINYINT(1) NULL DEFAULT '0',
  `vessel` TINYINT(1) NULL DEFAULT '0',
  `rda` TINYINT(1) NULL DEFAULT '0',
  `agency` TINYINT(1) NULL DEFAULT '0',
  `port` TINYINT(1) NULL DEFAULT '0',
  `employer` TINYINT(1) NULL DEFAULT '0',
  `vehicles` TINYINT(1) NULL DEFAULT '0',
  `users` TINYINT(1) NULL DEFAULT '0',
  `reports` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`group_id`),
  CONSTRAINT `fk_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `sgcvs`.`groups` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sgcvs`.`remarks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sgcvs`.`remarks` ;

CREATE TABLE IF NOT EXISTS `sgcvs`.`remarks` (
  `charge_id` INT(11) NOT NULL,
  `nautical_chart` TINYINT(1) NULL DEFAULT '0',
  `join_request` TINYINT(1) NULL DEFAULT '0',
  `feedback` TINYINT(1) NULL DEFAULT '0',
  `medicines` TINYINT(1) NULL DEFAULT '0',
  `boat_vs` TINYINT(1) NULL DEFAULT '0',
  `estimate_vs` TINYINT(1) NULL DEFAULT '0',
  `boat_client` TINYINT(1) NULL DEFAULT '0',
  `estimate_client` TINYINT(1) NULL DEFAULT '0',
  `others` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`charge_id`),
  CONSTRAINT `fk_charge_id`
    FOREIGN KEY (`charge_id`)
    REFERENCES `sgcvs`.`feeds` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
