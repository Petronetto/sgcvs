// var elixir = require('laravel-elixir');

// elixir(function(mix) {
//     mix.sass('app.scss');
// });

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cssMin = require('gulp-css');

gulp.task('css', function(){
    gulp.src([
        './public/assets/css/**/*.css'
    ])
    .pipe(concat('app.css'))
    .pipe(cssMin())
    .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('scripts', function(){
    gulp.src([
        './public/assets/js/**/*.js',
    ])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/assets/js'));
});

gulp.task('default', ['css', 'scripts']);
