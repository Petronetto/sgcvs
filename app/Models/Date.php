<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable = [
        'invoice_id',
        'eta',
        'etb',
        'ets',
        'supply',
        'first_charge',
        'second_charge',
        'third_charge',
        'estimated',
        'payment',
        'payment_date',
        'hour',
    ];

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }

    /**
     * Set date to format yyyy-mm-dd
     */
    public function setEtaAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['eta'] = NULL;
        } else {
            $this->attributes['eta'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setEtbAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['etb'] = NULL;
        } else {
            $this->attributes['etb'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setEtsAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['ets'] = NULL;
        } else {
            $this->attributes['ets'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setFirstChargeAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['first_charge'] = NULL;
        } else {
            $this->attributes['first_charge'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setSecondChargeAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['second_charge'] = NULL;
        } else {
            $this->attributes['second_charge'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setThirdChargeAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['third_charge'] = NULL;
        } else {
            $this->attributes['third_charge'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setSupplyAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['supply'] = NULL;
        } else {
            $this->attributes['supply'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setEstimatedAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['estimated'] = NULL;
        } else {
            $this->attributes['estimated'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }

    public function setPaymentDateAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['payment_date'] = NULL;
        } else {
            $this->attributes['payment_date'] = \DateTime::createFromFormat('d/m/Y', $value);
        }
    }


    /**
     * Return null or formated date time
     */
    public function getEtaAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getEtbAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getEtsAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getFirstChargeAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getSecondChargeAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getThirdChargeAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getSupplyAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getEstimatedAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    public function getPaymentDateAttribute($value)
    {
        return  $value != 0 ? date('d/m/Y', strtotime($value)) : NULL;
    }

    /**
     * Return the First Chat
     */
    public function getFirstChatAttribute($value)
    {
        $supply  = $this->attributes['supply'];
        $payment = $this->attributes['payment']+5;

        return  date('d/m/Y', strtotime("$supply + $payment days"));
    }

}
