<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'cpf', 'rg', 'cellphone', 'locale_id'];

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }

    /**
     * Set value to uppercase
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    /**
     * Relationship with Invoice Locale
     */
    public function locale()
    {
        return $this->belongsTo('App\Models\Locale');
    }
}
