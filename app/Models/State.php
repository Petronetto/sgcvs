<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * Relationship with Invoice Model
     */
     public function invoice()
     {
         return $this->hasMany('App\Models\Invoice');
     }

    /**
     * Relationship with Invoice Model
     */
     public function port()
     {
         return $this->hasMany('App\Models\Port');
     }
}
