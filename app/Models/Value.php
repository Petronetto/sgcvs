<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id',
        'gross_1',
        'discount_1',
        'gross_2',
        'discount_2',
        'wo',
        'received',
        'reference',
        'payment_remarks',
        'boat_cost',
        'stevedore_cost',
        'transport',
        'boat_cost_real',
        'stevedore_cost_real',
        'purch_value_nfe',
        'freight',
        'pending_cost',
        'pending_cost_obs',
    ];

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }

    /**
     * format the numbers
     */
    public function getGross1Attribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getGross2Attribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getWoAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getReceivedAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getReferenceAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getBoatCostAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getStevedoreCostAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getTransportAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getBoatCostRealAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getStevedoreCostRealAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getPurchValueNfeAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    public function getFreightAttribute($value)
    {
        return number_format($value, 2, '.', '');
    }

    /**
     * Return the sum of Operational Charges
     */
    public function getOperationalChargesAttribute($value)
    {
        $total = (
            $this->attributes['stevedore_cost'] +
            $this->attributes['boat_cost'] +
            $this->attributes['transport']
        );

        return number_format($total, 2, '.', '');
    }

    /**
     * Return the sum of Operational Charges
     */
    public function getTotalRealAttribute($value)
    {
        $total = (
            $this->attributes['stevedore_cost_real'] +
            $this->attributes['boat_cost_real'] +
            $this->attributes['freight'] +
            $this->attributes['purch_value_nfe']
        );

        return number_format($total, 2, '.', '');
    }

    /**
     * Return the sum of Out Balance
     */
    public function getTotalAttribute($value)
    {
        $total_discount_1 = $this->attributes['gross_1'] * $this->attributes['discount_1'];
        $total_discount_2 = $this->attributes['gross_2'] * $this->attributes['discount_2'];

        $total = (
            $total_discount_1 +
            $total_discount_2 +
            $this->attributes['stevedore_cost'] +
            $this->attributes['boat_cost'] +
            $this->attributes['transport'] +
            $this->attributes['wo']
        );

        return number_format($total, 2, '.', '');
    }

    /**
     * Return the sum of Out Balance
     */
    public function getOutBalanceAttribute($value)
    {
        $total_discount_1 = $this->attributes['gross_1'] * $this->attributes['discount_1'];
        $total_discount_2 = $this->attributes['gross_2'] * $this->attributes['discount_2'];

        $total = (
            $total_discount_1 +
            $total_discount_2 +
            $this->attributes['stevedore_cost'] +
            $this->attributes['boat_cost'] +
            $this->attributes['transport'] +
            $this->attributes['wo']
        );

        $out_balance = $this->attributes['received'] - $total;

        return number_format($out_balance, 2, '.', '');
    }

    /**
     * Value of discounts
     */
     public function getValueDisc1Attribute($value)
     {
         $total = $this->attributes['gross_1'] * $this->attributes['discount_1'];

         return number_format($total, 2, '.', '');
     }

     public function getValueDisc2Attribute($value)
     {
         $total = $this->attributes['gross_2'] * $this->attributes['discount_2'];

         return number_format($total, 2, '.', '');
     }

     /**
      * Return percent discount
      */
     public function getPercent1Attribute($value)
     {
         $total = 100 - ($this->attributes['discount_1'] * 100);

         return $total;
     }

     public function getPercent2Attribute($value)
     {
         $total = 100 - ($this->attributes['discount_2'] * 100);

         return $total;
     }
}
