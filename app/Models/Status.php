<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }

}
