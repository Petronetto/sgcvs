<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Remark extends Model
{
    protected $fillable = [
        'invoice_id',
        'nautical_chart',
        'join_request',
        'feedback',
        'medicines',
        'boat_vs',
        'stowage_vs',
        'boat_client',
        'stowage_client',
        'other'
    ];
    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }

    /**
     * Return if the invoice has one ou more remarks
     */
    public function getHasRemarksAttribute($value)
    {
        if (
            $this->attributes['nautical_chart'] != 0 ||
            $this->attributes['join_request']   != 0 ||
            $this->attributes['feedback']       != 0 ||
            $this->attributes['medicines']      != 0 ||
            $this->attributes['boat_vs']        != 0 ||
            $this->attributes['stowage_vs']     != 0 ||
            $this->attributes['boat_client']    != 0 ||
            $this->attributes['stowage_client'] != 0 ||
            $this->attributes['other']          != ""
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
