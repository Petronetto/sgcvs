<?php

namespace App\Http\Requests\Vessel;

use App\Http\Requests\Request;

class StoreVesselRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|unique:vessels,name,'.$this->route('vessel'),
            'email' => 'required|email',
        ];
    }
}
