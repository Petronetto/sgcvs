<?php

namespace App\Http\Requests\Vessel;

use App\Http\Requests\Request;
use App\Models\Vessel;

class DeleteVesselRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $vessel = Vessel::findOrFail($this->route('vessel'));

        if (!$vessel->invoice->isEmpty())
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You can not delete vessel with ivoices. Please delete all invoices related to this vessel.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
