<?php

namespace App\Http\Requests\Agency;

use App\Http\Requests\Request;
use App\Models\Agency;

class DeleteAgencyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $agency = Agency::findOrFail($this->route('agency'));

        if (!$agency->invoice->isEmpty())
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You can not delete agency with ivoices. Please delete all invoices related to this agency.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
