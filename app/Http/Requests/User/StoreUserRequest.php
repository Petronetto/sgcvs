<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['required'],
            'username'  => ['required', 'unique:users'],
            'email'     => ['required'],
            'group_id'  => ['required'],
            'locale_id' => ['required'],
            'password'  => ['required']
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required'  => 'The login field is required',
            'email.required'     => 'The e-mail field is required.',
            'group_id.required'  => 'The group field is required.',
            'locale_id.required' => 'The locale field is required.'
        ];
    }
}
