<?php

namespace App\Http\Requests\Employer;

use App\Http\Requests\Request;

class StoreEmployerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cpf'  => 'unique:employers,cpf,'.$this->route('employer').',id,locale_id,'.auth()->user()->locale_id,
            'rg'   => 'unique:employers,rg,'.$this->route('employer').',id,locale_id,'.auth()->user()->locale_id,
        ];
    }
}
