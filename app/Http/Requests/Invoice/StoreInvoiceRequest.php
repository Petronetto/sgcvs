<?php

namespace App\Http\Requests\Invoice;

use App\Http\Requests\Request;

class StoreInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'    => 'required',
            'groups'    => 'required',
            'vessel_id' => 'required',
            'port_id'   => 'required',
            'agency_id' => 'required',
            'eta'       => 'required|date_format:d/m/Y',
            'etb'       => 'date_format:d/m/Y',
            'ets'       => 'date_format:d/m/Y',
            'rda_id'    => 'required',
            'payment'   => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'number.required'    => 'The Invoice field is required',
            'groups.required'    => 'The Agency field is required.',
            'vessel_id.required' => 'The Vessel field is required.',
            'port_id.required'   => 'The Port field is required.',
            'agency_id.required' => 'The Agency field is required.',
            'eta.required'       => 'The ETA date field is required.',
            'eta.date_format'    => 'Invalid date format, must be dd/mm/yyyy.',
            'etb.date_format'    => 'Invalid date format, must be dd/mm/yyyy.',
            'ets.date_format'    => 'Invalid date format, must be dd/mm/yyyy.',
            'rda_id.required'    => 'The RDA is required.',
            'payment.required'   => 'The Payment terms is required.',
        ];
    }
}
