<?php

namespace App\Http\Requests\Invoice;

use App\Http\Requests\Request;
use Input;

class UpdateInvoiceRequest extends Request
{
    /**
     * Check if a Supply date was provided if the status was changed to Supplied..
     *
     * @return bool
     */
    public function authorize()
    {
        if(Input::get('status_id') == 2 && empty(Input::get('supply'))) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Messa if authorize is denied.
     *
     * @return bool
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You must inform a Supply Date before change the status to Supplied.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eta'       => 'date_format:d/m/Y',
            'etb'       => 'date_format:d/m/Y',
            'ets'       => 'date_format:d/m/Y',
            'supply'    => 'date_format:d/m/Y',
            'estimated' => 'date_format:d/m/Y',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'eta.date_format'       => 'Invalid date format, must be dd/mm/yyyy.',
            'etb.date_format'       => 'Invalid date format, must be dd/mm/yyyy.',
            'ets.date_format'       => 'Invalid date format, must be dd/mm/yyyy.',
            'supply.date_format'    => 'Invalid date format, must be dd/mm/yyyy.',
            'estimated.date_format' => 'Invalid date format, must be dd/mm/yyyy.',
        ];
    }
}
