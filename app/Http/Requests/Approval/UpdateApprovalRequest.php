<?php

namespace App\Http\Requests\Approval;

use App\Http\Requests\Request;

class UpdateApprovalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'    => 'required',
            'groups'    => 'required',
            'vessel_id' => 'required',
            'port_id'   => 'required',
            'agency_id' => 'required',
            'eta'       => 'required|date_format:d/m/Y',
            'etb'       => 'date_format:d/m/Y',
            'ets'       => 'date_format:d/m/Y',
            'rda_id'    => 'required',
            'payment'   => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'number.required'    => 'The Invoice is required',
            'groups.required'    => 'The Agency is required.',
            'vessel_id.required' => 'The Vessel is required.',
            'port_id.required'   => 'The Port is required.',
            'agency_id.required' => 'The Agency is required.',
            'eta.required'       => 'The ETA date is required.',
            'eta.date_format'    => 'Invalid date format, must be dd/mm/yyyy.',
            'etb.date_format'    => 'Invalid date format, must be dd/mm/yyyy.',
            'ets.date_format'    => 'Invalid date format, must be dd/mm/yyyy.',
            'rda_id.required'    => 'The RDA is required.',
            'payment.required'   => 'The Payment terms is required.',
        ];
    }
}
