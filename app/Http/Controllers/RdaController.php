<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Rda;
use Gate;

class RdaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('rda')) {
            return redirect(route('home'));
        }

        $rdas = Rda::all();

        return view('rda.index')->with('rdas', $rdas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Rda\StoreRdaRequest $request)
    {
        if (Gate::denies('rda')) {
            return redirect(route('home'));
        }

        $rda = new Rda;

        $rda->create($request->all());

        return redirect(route('rda.index'))->with('status', 'Rda saved with success.');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('rda')) {
            return redirect(route('home'));
        }

        $rda = Rda::findOrFail($id);

        return view('rda.edit')->with('rda', $rda);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Rda\StoreRdaRequest $request, $id)
    {
        if (Gate::denies('rda')) {
            return redirect(route('home'));
        }

        $rda = Rda::findOrFail($id);

        $rda->fill($request->all())->save();

        return redirect()->back()->with('status', 'RDA/CO-RDA updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\Rda\DeleteRdaRequest $request, $id)
    {
        if (Gate::denies('rda')) {
            return redirect(route('home'));
        }

        $rda = Rda::findOrFail($id);

        $rda->delete();

        return redirect()->back()->with('status', 'RDA/CO-RDA deleted with success.');
    }
}
