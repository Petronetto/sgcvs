<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Vessel;
use App\Models\Rda;
use App\Models\Port;
use App\Models\Agency;
use App\Models\Conclusion;
use Gate;

class HistoricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('stage', '=', 5)->get();

        return view('historic.index', compact('invoices'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        if ($invoice->stage != 5) {
            return redirect()->back();
        }

        return view('historic.show', compact('invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        if ($invoice->stage != 5) {
            return redirect()->back();
        }

        if ($request->open) {
            $invoice->stage = 3;
            $invoice->update();

            $data = [
                'invoice'     => $invoice,
                'rdas'        => Rda::All()->sortBy('name'),
                'vessels'     => Vessel::All()->sortBy('name'),
                'agencies'    => Agency::All()->sortBy('name'),
                'ports'       => Port::All()->sortBy('name'),
                'conclusions' => Conclusion::All(),
            ];

            return redirect(route('billing.edit',$invoice->id))->with($data);
        }
    }

}
