<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Vessel;
use Gate;

class VesselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessels = Vessel::all();

        return view('vessel.index')->with('vessels', $vessels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Vessel\StoreVesselRequest $request)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessel = new Vessel;

        $vessel->create($request->only('name', 'email'));

        return redirect(route('vessel.index'))->with('status', 'Vessel saved with success.');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessel = Vessel::findOrFail($id);

        return view('vessel.edit')->with('vessel', $vessel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Vessel\StoreVesselRequest $request, $id)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessel = Vessel::findOrFail($id);

        $vessel->fill($request->only('name', 'email'))->save();

        return redirect()->back()->with('status', 'Vessel updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\Vessel\DeleteVesselRequest $request, $id)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }
        
        $vessel = Vessel::findOrFail($id);

        $vessel->delete();

        return redirect()->back()->with('status', 'Vessel deleted with success.');
    }
}
