<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Value;
use App\Models\Remark;
use App\Models\Date;
use App\Models\Rda;
use App\Models\Vessel;
use App\Models\Agency;
use App\Models\Port;
use App\Models\Vehicle;
use App\Models\Employer;
use App\Models\Status;
use Gate;
use Validator;
use Mail;

use Illuminate\Support\Facades\Route;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::where('stage', '=', 1)
                  ->where('locale_id', '=', auth()->user()->locale_id)
                  ->get();

        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        return view('invoice.index')->with('invoices', $invoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-invoice')) {
            return redirect(route('home'));
        }

        $data = [
            'rdas'     => Rda::All()->sortBy('name'),
            'vessels'  => Vessel::All()->sortBy('name'),
            'agencies' => Agency::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'ports'    => Port::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
        ];

        return view('invoice.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Invoice\StoreInvoiceRequest $request, Invoice $i, Value $v, Remark $r, Date $d)
    {
        if (Gate::denies('create-invoice')) {
            return redirect(route('home'));
        }

        $i->fill($request->all());
        $v->fill($request->all());
        $r->fill($request->all());
        $d->fill($request->all());

        // Getting user Id
        $i->user_id = auth()->user()->id;
        // Getting the locale of the invoice
        $i->locale_id = auth()->user()->locale_id;

        $i->save();
        $i->value()->save($v);
        $i->remark()->save($r);
        $i->date()->save($d);

        return redirect(route('invoice.create'))->with('status', 'Invoice created with success.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        if ($invoice->stage != 1 || $invoice->locale_id != auth()->user()->locale_id) {
            return redirect()->back();
        }

        $data = [
            'invoice'   => $invoice,
            'rdas'      => Rda::All()->sortBy('name'),
            'vessels'   => Vessel::All()->sortBy('name'),
            'agencies'  => Agency::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'ports'     => Port::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'vehicles'  => Vehicle::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('plaque', 'asc')
                                  ->get(),
            'employers' => Employer::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'status'    => Status::All()->sortBy('name'),
        ];

        return view('invoice.edit')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dates($id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        if ($invoice->stage != 1 || $invoice->locale_id != auth()->user()->locale_id) {
            return redirect()->back();
        }

        return view('invoice.dates')->with('invoice', $invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function datesUpdate(Requests\Invoice\DatesUpdateInvoiceRequest $request, $id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);
        $remark  = Invoice::findOrFail($id)->remark;
        $date    = Invoice::findOrFail($id)->date;

        $remark->update($request->only('join_request'));
        $date->update($request->all());
        $invoice->update($request->all());

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Invoice\UpdateInvoiceRequest $request, $id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $i = Invoice::findOrFail($id);
        $v = Invoice::findOrFail($id)->value;
        $r = Invoice::findOrFail($id)->remark;
        $d = Invoice::findOrFail($id)->date;

        if(in_array($request->status_id, [2,3])) {
            $i->stage = 2;
        }

        $i->update($request->all());
        $v->update($request->all());
        $d->update($request->all());

        // Check if is a Commercial user to avoid the
        // Opracional Manager update an invoice and
        // setting null to all remarks.
        if (Gate::allows('create-invoice')) {
            $r->update($request->only(
                'nautical_chart',
                'join_request',
                'feedback',
                'medicines',
                'boat_vs',
                'stowage_vs',
                'boat_client',
                'stowage_client',
                'other'
            ));
        }

        // Send an e-mail if the Supply Dates was provided
        // and "Send E-mail Confirmation" is checked
        if ($request->supply && $request->confirmation) {
            try {
                Mail::send('emails.confirm', ['invoice' => $i], function ($m) use($i) {
                    if (auth()->user()->locale->id == 2) {
                        $m->from('supplier.rio@vitoriaship.com.br', 'Vitoria Ship Supplier');
                    } else {
                        $m->from('vitoriaship@vitoriaship.com.br', 'Vitoria Ship Supplier');
                    }

                    $m->to($i->vessel->email, $i->vessel->name);
                    if (auth()->user()->locale->id == 2) {
                        $m->cc('supplier.rio@vitoriaship.com.br', 'Vitoria Ship Supplier');
                    } else {
                        $m->cc('vitoriaship@vitoriaship.com.br', 'Vitoria Ship Supplier');
                    }
                    $m->cc($i->rda->email, $i->rda->name)
                        ->subject('Vitoria ship confirmation of supply MV '.$i->vessel->name);
                });
            } catch (Exception $e) {
                return redirect(route('invoice.index'))
                        ->with('error', 'Some error occured while try send the e-mail.');
            }
        }

        if ($i->stage == 2) {
            return redirect(route('invoice.index'))
                    ->with('status', 'Invoice updated with success.');
        }

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\Invoice\DeleteInvoiceRequest $request, $id)
    {
        if (Gate::denies('delete')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);
        $invoice->value()->delete();
        $invoice->remark()->delete();
        $invoice->date()->delete();
        $invoice->delete();

        return redirect()->back()->with('status', 'Invoice deleted with success.');
    }
}
