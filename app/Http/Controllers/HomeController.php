<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models;
use App\Models\Invoice;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         if (auth()->user()->group->id == 4) {
              return redirect(route('operational'));
         }

         return redirect(route('invoice.index'));
     }
}
