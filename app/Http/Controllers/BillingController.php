<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Vessel;
use App\Models\Rda;
use App\Models\Port;
use App\Models\Agency;
use App\Models\Conclusion;
use Gate;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('stage', '=', 3)->get();

        return view('billing.index', compact('invoices', 'invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        if ($invoice->stage != 3) {
            return redirect()->back();
        }

        $data = [
            'invoice'     => $invoice,
            'rdas'        => Rda::All()->sortBy('name'),
            'vessels'     => Vessel::All()->sortBy('name'),
            'agencies'    => Agency::where('locale_id', '=', $invoice->locale_id)
                                     ->orderBy('name')
                                     ->get(),
            'ports'       => Port::where('locale_id', '=', $invoice->locale_id)
                                     ->orderBy('name')
                                     ->get(),
            'conclusions' => Conclusion::All(),
        ];

        return view('billing.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        $i = Invoice::findOrFail($id);
        $v = Invoice::findOrFail($id)->value;
        $d = Invoice::findOrFail($id)->date;
        $r = Invoice::findOrFail($id)->remark;

        if($request->conclusion_id != 1) {
            $i->stage = 4;
        }

        // update Lawyer field
        $i->update($request->only('lawyer'));

        // update Pending Cost field
        $v->update($request->only('pending_cost'));

        $i->update($request->all());
        $v->update($request->all());
        $d->update($request->all());
        $r->update($request->only(
            'nautical_chart',
            'join_request',
            'feedback',
            'medicines',
            'boat_vs',
            'stowage_vs',
            'boat_client',
            'stowage_client',
            'other'
        ));

        if($request->conclusion_id != 1) {
            return redirect(route('billing.index'))->with('status', 'Invoice updated with success.');
        }

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }
}
