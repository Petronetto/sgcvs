<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Gate;

class SettlementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::all();

        return view('settlement.index', compact('invoices'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notSupplied()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('status_id', '=', 1)->get();

        return view('settlement.index', compact('invoices'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function supplied()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('status_id', '=', 2)->get();

        return view('settlement.index', compact('invoices'));
    }
}
