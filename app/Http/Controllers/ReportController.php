<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Rda;
use App\Models\Locale;
use Gate;
use DB;
use Input;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('report')) {
            return redirect(route('home'));
        }

        // Get all the years stored
        $years = DB::table('dates')->distinct()
                    ->select(DB::raw('YEAR(supply) as year'))
                    ->where('supply', '>', 1900)
                    ->orderBy('supply', 'ASC')
                    ->get();

        // Get RDA's ordered by name
        $rdas = Rda::orderBy('name', 'ASC')->get();

        // Get Locales ordered by name
        $locales = Locale::orderBy('name', 'ASC')->get();

        // Creating the query based on user input choices
        $query = Invoice::query();
        $query->whereIn('stage', [2,3,4,5]);
        if (Input::has('rda')) {
            $query->where('rda_id', Input::get('rda'));
        }

        if (Input::has('locale')) {
            $query->where('locale_id', Input::get('locale'));
        }

        if (Input::has('year')) {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('YEAR(supply) = '.Input::get('year'));
            });
        }

        if (Input::has('month')) {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('MONTH(supply) = '.Input::get('month'));
            });
        }

        $invoices = $query->get();

        return view('report.index', compact('years', 'rdas', 'locales', 'invoices'));
    }

}
