<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use Gate;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('vehicle')) {
            return redirect(route('home'));
        }

        $vehicles = Vehicle::where('locale_id', '=', auth()->user()->locale_id)->get();

        return view('vehicle.index')->with('vehicles', $vehicles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Vehicle\StoreVehicleRequest $request, Vehicle $vehicle)
    {
        if (Gate::denies('vehicle')) {
            return redirect(route('home'));
        }

        $vehicle->fill($request->only('model', 'plaque'));
        $vehicle->locale_id = auth()->user()->locale_id;
        $vehicle->save();

        return redirect(route('vehicle.index'))->with('status', 'Vehicle saved with success.');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('vehicle')) {
            return redirect(route('home'));
        }

        $vehicle = Vehicle::findOrFail($id);

        return view('vehicle.edit')->with('vehicle', $vehicle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Vehicle\StoreVehicleRequest $request, $id)
    {
        if (Gate::denies('vehicle')) {
            return redirect(route('home'));
        }

        $vehicle = Vehicle::findOrFail($id);

        $vehicle->fill($request->only('model', 'plaque'))->save();

        return redirect()->back()->with('status', 'Vehicle updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('vehicle')) {
            return redirect(route('home'));
        }

        $vehicle = Vehicle::findOrFail($id);

        $vehicle->delete();

        return redirect()->back()->with('status', 'Vehicle deleted with success.');
    }
}
