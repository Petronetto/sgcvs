<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Agency;
use Gate;


class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('agency')) {
            return redirect(route('home'));
        }

        $agencies = Agency::where('locale_id', '=', auth()->user()->locale_id)->get();

        return view('agency.index')->with('agencies', $agencies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Agency\StoreAgencyRequest $request, Agency $agency)
    {
        if (Gate::denies('agency')) {
            return redirect(route('home'));
        }

        $agency->fill($request->only('name', 'responsible', 'telephone', 'cellphone'));
        $agency->locale_id = auth()->user()->locale_id;
        $agency->save();

        return redirect(route('agency.index'))->with('status', 'Agency saved with success.');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('agency')) {
            return redirect(route('home'));
        }

        $agency = Agency::findOrFail($id);

        return view('agency.edit')->with('agency', $agency);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Agency\StoreAgencyRequest $request, $id)
    {
        if (Gate::denies('agency')) {
            return redirect(route('home'));
        }

        $agency = Agency::findOrFail($id);
        $agency->fill($request->only('name', 'responsible', 'telephone', 'cellphone'))->save();

        return redirect()->back()->with('status', 'Agency updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\Agency\DeleteAgencyRequest $request, $id)
    {
        if (Gate::denies('agency')) {
            return redirect(route('home'));
        }

        $agency = Agency::findOrFail($id);
        $agency->delete();

        return redirect()->back()->with('status', 'Agency deleted with success.');
    }
}
