<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Rda;
use App\Models\Vessel;
use App\Models\Agency;
use App\Models\Port;
use App\Models\Value;
use App\Models\Remark;
use App\Models\Status;
use Gate;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        // Get all invoices to line-up approval
        $invoiceLs = Invoice::where('stage', '=', 2)
                            ->where('locale_id', '=', auth()->user()->locale_id)
                            ->get();

        // Get all invoices to Billing approval
        $invoiceBs = Invoice::where('stage', '=', 4)->get();

        return view('approval.index', compact('invoiceLs', 'invoiceBs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lineUp($id)
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        if ($invoice->stage != 2) {
            return redirect()->back();
        }

        // Check if User locale is the same of the invoice
        if ($invoice->locale_id != auth()->user()->locale_id) {
            return redirect()->back();
        }

        $data = [
            'invoice'  => $invoice,
            'rdas'     => Rda::All()->sortBy('name'),
            'vessels'  => Vessel::All()->sortBy('name'),
            'agencies' => Agency::where('locale_id', '=', $invoice->locale_id)
                                ->orderBy('name')
                                ->get(),
            'ports'    => Port::where('locale_id', '=', $invoice->locale_id)
                              ->orderBy('name')
                              ->get(),
        ];

        return view('approval.line-up')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function billing($id)
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        // Send back if is wrong stage
        if ($invoice->stage != 4) {
            return redirect()->back();
        }

        $data = [
            'invoice'  => $invoice,
            'rdas'     => Rda::All()->sortBy('name'),
            'vessels'  => Vessel::All()->sortBy('name'),
            'agencies' => Agency::where('locale_id', '=', $invoice->locale_id)
                                ->orderBy('name')
                                ->get(),
            'ports'    => Port::where('locale_id', '=', $invoice->locale_id)
                              ->orderBy('name')
                              ->get(),
        ];

        return view('approval.billing')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Approval\UpdateApprovalRequest $request, $id)
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        $i = Invoice::findOrFail($id);
        $v = Invoice::findOrFail($id)->value;
        $d = Invoice::findOrFail($id)->date;

        // update Lawyer field
        $i->update($request->only('lawyer'));

        // update Pending Cost field
        $v->update($request->only('pending_cost'));

        // If is approved
        if($request->approval == 'yes') {
            // If Line-Up Approval
            if ($request->line_up) {
                // send to billing
                $i->stage = 3;
            }
            // If is Cancelled invoice or approved billing
            if ($i->status_id == 3 || $request->billing) {
                // send to historic
                $i->stage = 5;
            }

            $i->update($request->all());
            $v->update($request->all());
            $d->update($request->all());

            return redirect(route('approval.index'))->with('status', 'Invoice approved with success.');
        }

        // If is not approved
        if($request->approval == 'no') {

            if ($i->stage == 4) {
                // send to Billing and set Conclusion to "No"
                $i->stage = 3;
                $i->conclusion_id = 1;
            } else {
                // send to Line-up
                $i->stage = 1;
            }

            $i->update($request->all());
            $v->update($request->all());
            $d->update($request->all());

            return redirect(route('approval.index'))->with('status', 'Invoice rejected with success.');
        }

        // if is just update
        $i->update($request->all());
        $v->update($request->all());
        $d->update($request->all());

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }

}
