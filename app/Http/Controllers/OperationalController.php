<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;

class OperationalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices =  Invoice::join('dates', 'invoices.id', '=', 'dates.id')
                            ->where('invoices.locale_id', '=', auth()->user()->locale_id)
                            ->where('invoices.stage', '=', 1)
                            ->Where(function($query) {
                                $query->whereNotNull('dates.estimated')
                                      ->orWhereNotNull('dates.supply');
                            })
                            ->orderBy('dates.estimated')
                            ->get();

        return view('operational.index')->with('invoices', $invoices);
    }
}
