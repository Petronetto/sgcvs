<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Locale;
use App\Models\Group;
use Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $users    = User::all();
        $locales  = Locale::all();
        $groups   = Group::all();

        return view('user.index', compact('users', 'locales', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\User\StoreUserRequest $request)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user = new User;

        $user->create($request->only('name', 'username', 'email', 'group_id', 'locale_id', 'password'));

        return redirect(route('user.index'))->with('status', 'User saved with success.');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user     = User::findOrFail($id);
        $locales  = Locale::all();
        $groups   = Group::all();

        return view('user.edit', compact('user', 'locales', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\User\UpdateUserRequest $request, $id)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user = User::findOrFail($id);

        /**
         *  Check if the password was changed and update it if true
         */
        if(!empty($request->input('password'))) {
            $user->fill($request->all())->save();
        }
        else {
            $user->fill($request->except('password'))->save();
        }

        return redirect()->back()->with('status', 'User updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\User\DeleteUserRequest $request, $id)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user = User::findOrFail($id);

        if (!$user->invoice->isEmpty()) {
            return redirect()->back()->withErrors([
                'error' => 'This user has invoices related to him,
                you must delete all related invoices to proceed.'
            ]);
        }

        $user->delete();

        return redirect()->back()->with('status', 'User deleted with success.');
    }
}
