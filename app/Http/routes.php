<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Home
Route::get('/', 'HomeController@index')->name('home');

// Invoice CRUD
Route::resource('/invoice', 'InvoiceController');
Route::get('/invoice/date/{id}', 'InvoiceController@dates')->name('invoice.dates');
Route::put('/invoice/date/{id}', 'InvoiceController@datesUpdate')->name('invoice.datesUpdate');

// Authentication routes
Route::get('login', 'Auth\AuthController@getLogin')->name('auth.login');
Route::post('login', 'Auth\AuthController@postLogin')->name('auth.login');
Route::get('logout', 'Auth\AuthController@getLogout')->name('auth.logout');

// Password reset link request routes.
Route::get('password/email', 'Auth\PasswordController@getEmail')->name('password.email');
Route::post('password/email', 'Auth\PasswordController@postEmail')->name('password.email');

// Password reset routes
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset')->name('reset');
Route::post('password/reset', 'Auth\PasswordController@postReset')->name('reset');

// Vessels routes
Route::resource('vessel', 'VesselController');

// RDAs routes
Route::resource('rda', 'RdaController');

// Agencies routes
Route::resource('agency', 'AgencyController');

// Ports routes
Route::resource('port', 'PortController');
Route::get('port/data/{id}','PortController@data')->name('port.data');

// Employers routes
Route::resource('employer', 'EmployerController');

// Vehicles routes
Route::resource('vehicle', 'VehicleController');

// Users routes
Route::resource('user', 'UserController');

// Approval routes
Route::resource('approval', 'ApprovalController');
Route::get('approval/line-up/{id}', 'ApprovalController@lineUp')->name('approval.lineUp');
Route::get('approval/billing/{id}', 'ApprovalController@billing')->name('approval.billing');

// Billing routes
Route::resource('billing', 'BillingController');

// Historic routes
Route::resource('historic', 'HistoricController');

// Settlement routes
Route::get('settlement/all', 'SettlementController@all')->name('settlement.all');
Route::get('settlement/not-supplied', 'SettlementController@notSupplied')->name('settlement.notSupplied');
Route::get('settlement/supplied', 'SettlementController@supplied')->name('settlement.supplied');

// Report routes
Route::get('report', 'ReportController@index')->name('report');

// Operational list routes
Route::get('programacao', 'OperationalController@index')->name('operational');
