<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        $gate->define('line-up', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2, 3]);
        });

        $gate->define('create-invoice', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('delete', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('approval', function ($user) {
            return auth()->user()->group->id === 1;
        });

        $gate->define('billing', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('settlement', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('historic', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('report', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        /**
         * Records Menu
         */
        $gate->define('user', function ($user) {
            return auth()->user()->group->id === 1;
        });

        $gate->define('vessel', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('rda', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('port', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2]);
        });

        $gate->define('agency', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2, 3]);
        });

        $gate->define('employer', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2, 3]);
        });

        $gate->define('vehicle', function ($user) {
            return in_array(auth()->user()->group->id, [1, 2, 3]);
        });

    }
}
