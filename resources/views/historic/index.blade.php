@extends('main')

@section('title', 'Historic | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Historic</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container-fluid p-t2 m40">
        <div class="row">
            <div class="table-responsive">
                <table id="table" class="table table-bordered table-condensed table-hover data-table">
                    <thead>
                        <tr class="active">
                            <th>Supply Date</th>
                            <th>Invoice</th>
                            <th>NFe</th>
                            <th>Vessel</th>
                            <th>Port</th>
                            <th>R.D.A</th>
                            <th>CO-R.D.A</th>
                            <th>Supplied</th>
                            <th>Settled</th>
                            <th>Due Date</th>
                            <th class="opcoes">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                        <tr>
                            <td>{{ $invoice->date->supply }}</td>
                            <td>{{ $invoice->number }}</td>
                            <td>{{ $invoice->nfe }}</td>
                            <td>{{ $invoice->vessel->name }}</td>
                            <td>{{ $invoice->port->name }}</td>
                            <td>{{ $invoice->rda->name }}</td>
                            <td>{{ @$invoice->corda->name }}</td>
                            <td>{{ $invoice->status->name }}</td>
                            <td>{{ $invoice->conclusion->name }}</td>
                            <td>{{ $invoice->date->first_chat }}</td>
                            <td>
                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('historic.show', $invoice->id) }}"  class="btn btn-default border-right-n loading"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    </div>
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('invoice.destroy', $invoice->id) }}" class="btn btn-default"
                                            data-token="{{ csrf_token() }}"
                                            data-delete=""
                                            data-title="Are you sure?"
                                            data-message="Are you sure that you want delete this Invoice?"
                                            data-button-text="Yes, delete it!">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@include('footer')

@endsection
