@extends('main')

@section('title', 'Historic | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Historic</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
    <form action="{{ route('historic.update', $invoice->id) }}" method="post">
        {!! csrf_field() !!}
        <input name="_method" type="hidden" value="PUT">
        <div class="panel panel-danger">
            <div class="panel-body p-t7">
                <div class="row form-horizontal">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label class="col-sm-6 control-label">Supply status:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static"><span class="red">{{ $invoice->status->name }}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 p-t4">
                        <div class="form-group row">
                            <label class="col-sm-6 control-label">Billing status:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static"><span class="red">{{ $invoice->conclusion->name }}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informations</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Supply Date</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->supply }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Hour</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->hour }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="invoice" class="col-sm-3 control-label">Invoice</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->number }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nf" class="col-sm-3 control-label">NFe</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->nfe }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grupos" class="col-sm-3 control-label">Groups</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->groups }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="vessel" class="col-sm-3 control-label">Vessel</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->vessel->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="port" class="col-sm-3 control-label">Port</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->port->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="agency" class="col-sm-3 control-label">Agency</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->agency->name }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="eta" class="col-sm-3 control-label">ETA</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->eta }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="etb" class="col-sm-3 control-label">ETB</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->etb }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ets" class="col-sm-3 control-label">ETS</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->ets }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rda" class="col-sm-3 control-label">R.D.A</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->rda->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="corda" class="col-sm-3 control-label">CO-R.D.A</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ @$invoice->corda->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pterms" class="col-sm-3 control-label">Payment terms</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->payment }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fchat" class="col-sm-3 control-label">Due Date</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->first_chat }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Values</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="val_gross_1" class="col-sm-3 control-label">Val. Gross 1 (USD)</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->gross_1 }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="discount_1" class="col-sm-3 control-label"> % Disc. 1</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->percent_1 }}%</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_discount_1" class="col-sm-3 control-label">Val. Disc. 1 (USD)</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->value_disc_1 }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="val_gross_2" class="col-sm-3 control-label">Val. Gross 2 (USD)</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->gross_2 }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="discount_2" class="col-sm-3 control-label">% Disc. 2</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->percent_2 }}%</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_discount_2" class="col-sm-3 control-label">Val. Disc. 2 (USD)</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->value_disc_2 }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="val_wt_disc" class="col-sm-3 control-label">USD Val. W/O Disc</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->wo }}</p>
                            </div>
                        </div>
                        <!-- Esses 2 campos só aparecem se o PORTO informado acima, for TRUE para LANCHA E ESTIVA -->
                        <div class="form-group row">
                            <label for="lancha" class="col-sm-3 control-label">Boat Service</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->boat_cost }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="estiva" class="col-sm-3 control-label">Stevedore</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->stevedore_cost }}</p>
                            </div>
                        </div>
                        <!-- Esses 2 campos só aparecem se o PORTO informado acima, for TRUE para LANCHA E ESTIVA -->
                        <div class="form-group row">
                            <label for="tem_transporte" class="col-sm-3 control-label">Transportation</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->transport }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 p-t4">
                        <div class="form-group row">
                            <label for="areceber" class="col-sm-3 control-label">Total</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->total }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="received_value" class="col-sm-3 control-label">Received value</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->received }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="out_balance" class="col-sm-3 control-label">Out Balance</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->out_balance }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="valor_em_real" class="col-sm-3 control-label">R$ Reference</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->reference }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="1charge" class="col-sm-3 control-label">1ª Charge</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->first_charge }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="2charge" class="col-sm-3 control-label">2ª Charge</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->second_charge }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="3charge" class="col-sm-3 control-label">3ª Charge</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->third }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ta_pagamento" class="col-sm-3 control-label">Payment Date</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->payment_date }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ta_pagamento" class="col-sm-3 control-label">Payment Remarks</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->payment_remarks }}</p>
                            </div>
                        </div>
                        <!-- CONDIÇÃO. Caso o valor do checkbox LAWYER seja true, a resposta é SIM, else NÃO. -->
                        <div class="form-group row">
                            <label for="ta_pagamento" class="col-sm-3 control-label">Lawyer</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->lawyer ? 'Yes' : 'No' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cost Values</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="val_nf" class="col-sm-3 control-label">Purch. Value NFe</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">R${{ $invoice->value->purch_value_nfe }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="val_s_nf" class="col-sm-3 control-label">Boat Service</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">R${{ $invoice->value->boat_cost_real }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="estoque" class="col-sm-3 control-label">Stevedore</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">R${{ $invoice->value->stevedore_cost_real }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="estoque" class="col-sm-3 control-label">Freight</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">R${{ $invoice->value->freight }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_compra" class="col-sm-3 control-label">Total</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">R${{ $invoice->value->total_real }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 p-t4">
                        <div class="form-group row">
                            <label for="total_compra" class="col-sm-3 control-label">Vehicle</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ @$invoice->vehicle->plaque }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_compra" class="col-sm-3 control-label">Employers</label>
                            <div class="col-sm-9">
                                <ul class="lista-status">
                                    <li>{{ @$invoice->employerOne->name }}</li>
                                    @if($invoice->employerTwo)
                                        <li>{{ $invoice->employerTwo->name }}</li>
                                    @endif
                                    @if($invoice->employerThree)
                                        <li>{{ $invoice->employerThree->name }}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_compra" class="col-sm-3 control-label">Remarks</label>
                            <div class="col-sm-9">
                                <ul class="lista-status">
                                    @if($invoice->remark->nautical_chart) <li>Carta Náutica</li> @endif
                                    @if($invoice->remark->join_request) <li>Solicitação de ingresso</li> @endif
                                    @if($invoice->remark->feedback) <li>Feedback Form/Invoice</li> @endif
                                    @if($invoice->remark->medicines) <li>Medicamentos</li> @endif
                                    @if($invoice->remark->boat_vs) <li>Lancha via VS</li> @endif
                                    @if($invoice->remark->stowage_vs) <li>Estiva via VS</li> @endif
                                    @if($invoice->remark->boat_client) <li>Lancha via agência</li> @endif
                                    @if($invoice->remark->stowage_client) <li>Estiva via agência</li> @endif
                                    @if($invoice->remark->other) <li>{{ $invoice->remark->other }}</li> @endif
                                </ul>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_compra" class="col-sm-3 control-label">Observations</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->obs }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer clearfix">
                <div class="col-sm-12  text-center">
                    <p>By clicking OPEN, the vessel line will automatically return to BILLING tab.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-10">
                </div>
                <div class="col-sm-2">
                    <button name="open" class="btn btn-primary btn-lg loading" value="true" type="submit">OPEN</button>
                </div>
            </div>
        </div>
    </form>
    </div>

@include('footer')

@endsection
