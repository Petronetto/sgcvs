<!-- Bootstrap core JavaScript # scripts #-->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/jquery-ui-1.10.0.custom.min.js"></script>
<script src="/assets/js/jquery.mask.js"></script>
<script src="/assets/js/fancy/jquery.fancybox.js"></script>
<script src="/assets/js/sweetalert.min.js"></script>
<script src="/media/js/jquery.dataTables.min.js"></script>
<script src="/media/js/moment.min.js"></script>
<script src="/media/js/datetime-moment.js"></script>
<script src="/media/js/dataTables.bootstrap.min.js"></script>
<script src="/media/js/dataTables.buttons.min.js"></script>
<script src="/assets/js/jszip.min.js"></script>
<script src="/assets/js/pdfmake.min.js"></script>
<script src="/assets/js/vfs_fonts.js"></script>
<script src="/media/js/buttons.html5.min.js"></script>
<script src="/media/js/buttons.print.min.js"></script>
<script src="/assets/js/buttons.colVis.min.js"></script>
<script src="/assets/js/jquery.maskMoney.min.js"></script>
<script src="/assets/js/vue.min.js"></script>
<script src="/assets/js/alerts.js"></script>
<script src="/assets/js/vue-app.js"></script>
<script src="/assets/js/SGCVS.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
