<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <link rel="icon" href="/assets/images/favicon.ico">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <title>@yield('title')</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/jquery-ui-1.10.0.custom.css" />
        <link rel="stylesheet" href="/assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="/assets/css/ie10-viewport-bug-workaround.css">
        <link rel="stylesheet" href="/assets/js/fancy/jquery.fancybox.css"/>
        <link rel="stylesheet" href="/assets/css/style.css">
        <!-- <link href="/media/css/jquery.dataTables.min.css"/> -->
        <link rel="stylesheet" href="/media/css/dataTables.bootstrap.min.css"/>
        <link rel="stylesheet" href="/media/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" href="/media/css/buttons.dataTables.min.css"/>
        <link rel="stylesheet" href="/assets/css/sweetalert.css"/>
        <!--[if IE 7]>
        <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/assets/css/jquery.ui.1.10.0.ie.css"/>
        <![endif]-->
    </head>
    <body>

        <div class="loader"></div>

        @yield('navbar')

        @yield('content')

        @include('scripts')

    </body>
</html>
