@extends('main')

@section('title', 'Billing | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Billing</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container-fluid p-t2 m40">
        <div class="row">
            @include('messages')
            <div class="table-responsive">
                <table id="billing" class="table table-bordered table-condensed table-hover data-table">
                    <thead>
                        <tr class="active">
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-flag"></span>
                            </th>
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </th>
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-usd"></span>
                            </th>
                            <th>Supply Date</th>
                            <th>Invoice</th>
                            <th>NFe</th>
                            <th>Vessel</th>
                            <th>Port</th>
                            <th>R.D.A</th>
                            <th>CO-R.D.A</th>
                            <th>Payment Terms</th>
                            <th>Locale</th>
                            <th>Due Date</th>
                            <th class="opcoes">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                        <tr @if($invoice->value->total == 0) class="alert alert-success" @endif
                            @if($invoice->value->pending_cost) class="alert alert-success" @endif>
                            <td>
                            @if($invoice->date->first_charge)
                                <span class="glyphicon glyphicon-flag"> </span>
                            @endif
                            </td>
                            <td>
                            @if($invoice->lawyer)
                                <span class="glyphicon glyphicon-eye-open"> </span>
                            @endif
                            </td>
                            <td>
                            @if($invoice->value->pending_cost)
                                <span class="glyphicon glyphicon-usd"> </span>
                            @endif
                            </td>
                            <td>{{ $invoice->date->supply }}</td>
                            <td>{{ $invoice->number }}</td>
                            <td>{{ $invoice->nfe }}</td>
                            <td>{{ $invoice->vessel->name }}</td>
                            <td>{{ $invoice->port->name }}</td>
                            <td>{{ $invoice->rda->name }}</td>
                            <td>{{ @$invoice->corda->name}}</td>
                            <td>{{ $invoice->date->payment }}</td>
                            <td>{{ $invoice->locale->name }}</td>
                            <td>{{ $invoice->date->first_chat }}</td>
                            <td>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('billing.edit', $invoice->id) }}" class="btn btn-default border-right-n loading">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                        </a>
                                    </div>
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('invoice.destroy', $invoice->id) }}" class="btn btn-default"
                                            data-token="{{ csrf_token() }}"
                                            data-delete=""
                                            data-title="Are you sure?"
                                            data-message="Are you sure that you want delete this Invoice?"
                                            data-button-text="Yes, delete it!">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <span class="alert alert-success" role="info">
                <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                Pending Costs
            </span>
            <span class="alert alert-info" role="info">
                <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                Charge under process
            </span>
            <span class="alert alert-info" role="info">
                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                Lawyer action
            </span>
        </div>
    </div>

@include('footer')

@endsection
