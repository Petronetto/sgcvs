@extends('main')

@section('title', 'Port | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Records
            <span class="font-l">/ Port</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('port.store') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-body p-t7">
                <div class="row form-horizontal">
                    <div class="col-sm-3">
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 control-label">
                                <span class= "red">*</span> Name
                            </label>
                            <div class="col-sm-8">
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}" uppercase>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 p-t4">
                        <div class="form-group row">
                            <label for="state_id" class="col-sm-5 control-label"><span class= "red">*</span> UF</label>
                            <div class="col-sm-7">
                                <select name="state_id" class="form-control">
                                    @foreach($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->abbr }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="stevedore" class="col-sm-6 control-label">
                                <span class= "red">*</span> Stevedore
                            </label>
                            <div class="col-sm-6">
                                <select name="stevedore" class="form-control">
                                    <option value="1">YES</option>
                                    <option value="0">NO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 p-t4">
                        <div class="form-group row">
                            <label for="boat" class="col-sm-5 control-label">
                                <span class= "red">*</span> Boat
                            </label>
                            <div class="col-sm-7">
                                <select name="boat" class="form-control">
                                    <option value="1">YES</option>
                                    <option value="0">NO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary btn-block loading" type="submit" value="SAVE">
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Name</th>
                        <th>UF</th>
                        <th>Stevedore</th>
                        <th>Boat</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ports as $port)
                    <tr>
                        <td>{{ $port->name }}</td>
                        <td>{{ $port->state->abbr }}</td>
                        <td>{{ ($port->stevedore == 1 ? "YES":"NO") }}</td>
                        <td>{{ ($port->boat == 1 ? "YES":"NO") }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('port.edit', $port->id) }}" class="btn btn-default iframe fancybox.iframe border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('port.destroy', $port->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-delete=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete the port {{ $port->name }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('footer')

@endsection
