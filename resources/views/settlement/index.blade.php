@extends('main')

@section('title', 'Settlement | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Settlement</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

<div class="container-fluid p-t2 m40">
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Supply Date</th>
                        <th>Status</th>
                        <th>Invoice</th>
                        <th>Locale</th>
                        <th>Vessel</th>
                        <th>Port</th>
                        <th>R.D.A</th>
                        <th>CO-R.D.A</th>
                        <th>Payment <br> Terms</th>
                        <th>Due Date</th>
                        <th>Val. Gross <br>(USD)</th>
                        <th>% Disc.</th>
                        <th>Val. Disc. <br>(USD)</th>
                        <th>Val. Gross 2 <br>(USD)</th>
                        <th>% Disc.</th>
                        <th>Val. Disc. <br>(USD)</th>
                        <th>USD Val. W/O <br> Disc</th>
                        <th>Operational / <br>Charges</th>
                        <th>Val. Liquid <br>(USD)</th>
                        <th>Received <br> Value</th>
                        <th>Out <br> Balance</th>
                        <th>R$ <br> Reference</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invoices as $invoice)
                    <tr>
                        <td>{{ $invoice->date->supply }}</td>
                        <td>{{ $invoice->status->name }}</td>
                        <td>{{ $invoice->number }}</td>
                        <td>{{ $invoice->locale->name }}</td>
                        <td>{{ $invoice->vessel->name }}</td>
                        <td>{{ $invoice->port->name }}</td>
                        <td>{{ $invoice->rda->name }}</td>
                        <td>{{ @$invoice->corda->name }}</td>
                        <td>{{ $invoice->date->payment }}</td>
                        <td>{{ $invoice->date->first_chat }}</td>
                        <td>{{ $invoice->value->gross_1 }}</td>
                        <td>{{ $invoice->value->percent_1 }}%</td>
                        <td>{{ $invoice->value->value_disc_1 }}</td>
                        <td>{{ $invoice->value->gross_2 }}</td>
                        <td>{{ $invoice->value->percent_2 }}%</td>
                        <td>{{ $invoice->value->value_disc_2 }}</td>
                        <td>{{ $invoice->value->wo }}</td>
                        <td>{{ $invoice->value->operational_charges }}</td>
                        <td>{{ $invoice->value->total }}</td>
                        <td>{{ $invoice->value->received }}</td>
                        <td>{{ $invoice->value->out_balance }}</td>
                        <td>{{ $invoice->value->reference }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('footer')

@endsection
