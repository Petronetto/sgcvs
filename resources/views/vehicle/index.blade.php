@extends('main')

@section('title', 'Vehicle | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Records
            <span class="font-l">/ Vehicle</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('vehicle.store') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-body p-t7">
                <div class="row form-horizontal">
                    <div class="col-sm-5">
                        <div class="form-group row">
                            <label for="model" class="col-sm-3 control-label"><span class= "red">*</span> Model</label>
                            <div class="col-sm-9">
                                <input name="model" type="text" class="form-control" value="{{ old('model') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 p-t4">
                        <div class="form-group row">
                            <label for="plaque" class="col-sm-3 control-label"><span class= "red">*</span> Plaque</label>
                            <div class="col-sm-9">
                                <input name="plaque" id="plaque" type="text" class="form-control plaque" value="{{ old('plaque') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary btn-block loading" type="submit" value="SAVE">
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Model</th>
                        <th>Plaque</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vehicles as $vehicle)
                    <tr>
                        <td>{{ $vehicle->model }}</td>
                        <td>{{ $vehicle->plaque }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('vehicle.edit', $vehicle->id) }}" class="btn btn-default iframe fancybox.iframe border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('vehicle.destroy', $vehicle->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-delete=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete the vehicle {{ $vehicle->plaque }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('footer')

@endsection
