@extends('main')

@section('title', 'Approvals | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Approvals</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container-fluid p-t2">
        <div class="row">
            <h3 class="sub-header"> Line-up</h3>
            @include('messages')
            <div class="table-responsive">
                <table id="line-up" class="table table-bordered table-condensed table-hover data-table">
                    <thead>
                        <tr class="active">
                            <th>Supply Date</th>
                            <th>Invoice</th>
                            <th>Vessel</th>
                            <th>Port</th>
                            <th>R.D.A</th>
                            <th>CO-R.D.A</th>
                            <th>Supplied</th>
                            <th class="opcoes">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoiceLs as $invoiceL)
                        <tr>
                            <td>{{ $invoiceL->date->supply ?: 'Not supplied' }}</td>
                            <td>{{ $invoiceL->number }}</td>
                            <td>{{ $invoiceL->vessel->name }}</td>
                            <td>{{ $invoiceL->port->name }}</td>
                            <td>{{ $invoiceL->rda->name }}</td>
                            <td>{{ @$invoiceL->corda->name }}</td>
                            <td>{{ $invoiceL->status->name }}</td>
                            <td>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('approval.lineUp', $invoiceL->id) }}" class="btn btn-default border-right-n loading">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                        </a>
                                    </div>
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('invoice.destroy', $invoiceL->id) }}" class="btn btn-default"
                                            data-token="{{ csrf_token() }}"
                                            data-delete=""
                                            data-title="Are you sure?"
                                            data-message="Are you sure that you want delete this Invoice?"
                                            data-button-text="Yes, delete it!">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="container-fluid p-t2">
        <div class="row">
            <h3 class="sub-header"> Billing</h3>
            <div class="table-responsive">
                <table id="billing" class="table table-bordered table-condensed table-hover data-table">
                    <thead>
                        <tr class="active">
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-flag"></span>
                            </th>
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </th>
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-usd"></span>
                            </th>
                            <th>Suply Date</th>
                            <th>Invoice</th>
                            <th>Vessel</th>
                            <th>Port</th>
                            <th>R.D.A</th>
                            <th>CO-R.D.A</th>
                            <th>Charged</th>
                            <th class="opcoes">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoiceBs as $invoiceB)
                        <tr @if($invoiceB->value->pending_cost) class="alert alert-success" @endif>
                            <td>
                            @if($invoiceB->date->first_charge)
                                <span class="glyphicon glyphicon-flag"> </span>
                            @endif
                            </td>
                            <td>
                            @if($invoiceB->lawyer)
                                <span class="glyphicon glyphicon-eye-open"> </span>
                            @endif
                            </td>
                            <td>
                            @if($invoiceB->value->pending_cost)
                                <span class="glyphicon glyphicon-usd"> </span>
                            @endif
                            </td>
                            <td>{{ $invoiceB->date->supply }}</td>
                            <td>{{ $invoiceB->number }}</td>
                            <td>{{ $invoiceB->vessel->name }}</td>
                            <td>{{ $invoiceB->port->name }}</td>
                            <td>{{ $invoiceB->rda->name }}</td>
                            <td>{{ @$invoiceB->corda->name }}</td>
                            <td>{{ strtotime((str_replace('/', '.',$invoiceB->date->first_chat))) < date(strtotime('today')) ? 'Yes' : 'No'  }}</td>
                            <td>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('approval.billing', $invoiceB->id) }}" class="btn btn-default border-right-n loading">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                        </a>
                                    </div>
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('invoice.destroy', $invoiceB->id) }}" class="btn btn-default"
                                            data-token="{{ csrf_token() }}"
                                            data-delete=""
                                            data-title="Are you sure?"
                                            data-message="Are you sure that you want delete this Invoice?"
                                            data-button-text="Yes, delete it!">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <span class="alert alert-success" role="info">
                <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                Pending Costs
            </span>
            <span class="alert alert-info" role="info">
                <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                Charge under process
            </span>
            <span class="alert alert-info" role="info">
                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                Lawyer action
            </span>
        </div>
    </div>
@include('footer')

@endsection
