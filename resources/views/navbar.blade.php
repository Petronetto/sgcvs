@if(Auth::user()->group->id != 4)
<nav class="navbar navbar-default navbar-fixed-top mod-nav">
    <div class="container-fluid bemvindo">
        <div class="col-xs-6">
            <span class="align-left">
                Welcome
                <strong>{{ Auth::user()->name }}</strong>
            </span>
        </div>
        <div class="col-xs-6">
            <span class="align-right">
                <a class="loading" href="{{ route('auth.logout') }}">
                    <span class="glyphicon glyphicon-log-out"></span> Logout
                </a>
            </span>
        </div>
    </div>
    <div class="container p-t1">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="loading" href="{{ route('home') }}" class="navbar-brand pad0">
                <img class="img-responsive" src="/assets/images/logo.png" alt="Home">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right mod-nav-r">
                <li>
                    <a class="loading" href="{{ route('home') }}">
                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                        Line-up
                    </a>
                </li>
                @can('approval')
                <li>
                    <a class="loading" href="{{ route('approval.index') }}">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        Approvals
                    </a>
                </li>
                @endcan
                @can('billing')
                <li>
                    <a class="loading" href="{{ route('billing.index') }}">
                        <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                        Billing
                    </a>
                </li>
                @endcan



                @can('settlement')
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Settlement
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="loading" href="{{ route('settlement.all') }}">ALL</a>
                        </li>
                        <li>
                            <a class="loading" href="{{ route('settlement.supplied') }}">SUPPLIED</a>
                        </li>
                        <li>
                            <a class="loading" href="{{ route('settlement.notSupplied') }}">NOT SUPPLIED</a>
                        </li>
                    </ul>
                </li>
                @endcan


                @can('historic')
                <li>
                    <a class="loading" href="{{ route('historic.index') }}">
                        <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
                        Historic
                    </a>
                </li>
                @endcan
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Records
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @can('vessel')
                        <li>
                            <a class="loading" href="{{ route('vessel.index') }}">VESSEL</a>
                        </li>
                        @endcan
                        @can('rda')
                        <li>
                            <a class="loading" href="{{ route('rda.index') }}">R.D.A / CO-R.D.A</a>
                        </li>
                        @endcan
                        @can('agency')
                        <li>
                            <a class="loading" href="{{ route('agency.index') }}">AGENCY</a>
                        </li>
                        @endcan
                        @can('port')
                        <li>
                            <a class="loading" href="{{ route('port.index') }}">PORT</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        @endcan
                        @can('employer')
                        <li>
                            <a class="loading" href="{{ route('employer.index') }}">EMPLOYER</a>
                        </li>
                        @endcan
                        @can('vehicle')
                        <li>
                            <a class="loading" href="{{ route('vehicle.index') }}">VEHICLES</a>
                        </li>
                        @endcan
                        @can('user')
                        <li role="separator" class="divider"></li>
                        <li>
                            <a class="loading" href="{{ route('user.index') }}">USERS</a>
                        </li>
                        @endcan
                    </ul>
                </li>
                @can('report')
                <li class="bar-vert"><a class="loading" href="{{ route('report') }}"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Report</a></li>
                @endcan
                @can('create-invoice')
                <li><a href="{{ route('invoice.create') }}" class="novo loading"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> New</a></li>
                @endcan
            </ul>
        </div>
    </div>
</nav>
@endif
<div class="container-fluid bg-sgcvs">
    <div class="container">
        <div class="col-sm-6 col-xs-12">
            @yield('module')
        </div>
        <div class="col-xs-6 hidden-xs">
            <h3 class="text-right white">{{ Auth::user()->locale->name }} <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></h3>
        </div>
    </div>
</div>
