@extends('main')

@section('title', 'RDA/CO-RDA | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true">
            </span> Records <span class="font-l">/ R.D.A e CO-R.D.A</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('rda.store') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-body p-t7">
                <div class="row form-horizontal">
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <label for="nome_rda" class="col-sm-4 control-label">
                                <span class= "red">*</span> Name
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-t4">
                        <div class="form-group row">
                            <label for="email_rda" class="col-sm-3 control-label">
                                <span class="red">*</span> E-mail
                            </label>
                            <div class="col-sm-9">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 p-t4">
                        <div class="form-group row">
                            <label for="tem_estiva" class="col-sm-5 control-label">
                                <span class= "red">*</span> Type
                            </label>
                            <div class="col-sm-7">
                                <select name="type" class="form-control">
                                    <option value="1">SIGN</option>
                                    <option value="0">COB</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary btn-block loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Type</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rdas as $rda)
                    <tr>
                        <td>{{ $rda->name }}</td>
                        <td>{{ $rda->email }}</td>
                        <td>{{ ($rda->type == 1 ? "SIGN":"COB") }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('rda.edit', $rda->id) }}" class="btn btn-default iframe fancybox.iframe border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('rda.destroy', $rda->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-delete=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete {{ $rda->name }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('footer')

@endsection
