@extends('main')

@section('title', 'Edit RDA/CO-RDA')

@section('content')

<div class="container p-t1">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row form-horizontal">
            <form action="{{ route('rda.update', $rda->id) }}" method="post">
                {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PUT">
                <div class="col-sm-5">
                    @include('messages')
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <label for="nome_rda" class="col-sm-4 control-label">
                                <span class= "red">*</span> Name
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="name" value="{{ $rda->name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="email_rda" class="col-sm-5 control-label">
                                <span class= "red">*</span> E-mail
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="email" value="{{ $rda->email }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="tipo_rda" class="col-sm-5 control-label">
                                <span class= "red">*</span> Type
                            </label>
                            <div class="col-sm-7">
                                <select name="type" class="form-control">
                                    <option value="1" {{ ($rda->type == 1 ? "selected":"") }}>SIGN</option>
                                    <option value="0" {{ ($rda->type == 0 ? "selected":"") }}>COB</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>

@endsection
