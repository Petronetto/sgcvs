@extends('main')

@section('title', 'User | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Records
            <span class="font-l">/ Users</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('user.store') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-body p-t7">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 control-label">
                                <span class="red">*</span> Name
                            </label>
                            <div class="col-sm-9">
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-sm-3 control-label">
                                <span class="red">*</span> Login
                            </label>
                            <div class="col-sm-9">
                                <input name="username" type="text" class="form-control" value="{{ old('username') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="locale_id" class="col-sm-3 control-label">
                                <span class="red">*</span> Locale
                            </label>
                            <div class="col-sm-9">
                                <select name="locale_id" class="form-control">
                                    <option value="" selected>Select...</option>
                                    @foreach($locales as $locale)
                                        <option value="{{ $locale->id }}" {{ $locale->id == old('locale_id') ? 'selected' : '' }}>{{ $locale->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 control-label">E-mail</label>
                            <div class="col-sm-9">
                                <input name="email" type="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="group_id" class="col-sm-3 control-label">
                                <span class="red">*</span> Group
                            </label>
                            <div class="col-sm-9">
                                <select name="group_id" class="form-control">
                                    <option value="" selected>Select...</option>
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}" {{ $group->id == old('group_id') ? 'selected' : '' }}>{{ $group->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-3 control-label">
                                <span class="red">*</span> Password
                            </label>
                            <div class="col-sm-9">
                                <input name="password" type="password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-10 col-xs-12">
                        <input class="btn btn-primary btn-block loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Name</th>
                        <th>Login</th>
                        <th>E-mail</th>
                        <th>Group</th>
                        <th>Locale</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->group->name }}</td>
                        <td>{{ $user->locale->name }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('user.edit', $user->id) }}" class="btn btn-default iframe fancybox.iframe border-right-n" data-height="420">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('user.destroy', $user->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-delete
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete the user {{ $user->name }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('footer')

@endsection
