@extends('main')

@section('title', 'Edit Vessel')

@section('content')

<div class="container p-t1">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row form-horizontal">
            <form action="{{ route('vessel.update', $vessel->id) }}" method="post">
                {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PUT">
                <div class="col-sm-5">
                    @include('messages')
                    <div class="form-group row">
                        <label for="nome_vessel" class="col-sm-3 control-label">
                            <span class= "red">*</span> Name
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" value="{{ $vessel->name }}" uppercase>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 p-t4">
                     <div class="form-group row">
                        <label for="email_vessel" class="col-sm-3 control-label">
                            <span class= "red">*</span> E-mail
                        </label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" value="{{ $vessel->email }}" lowercase>
                        </div>
                    </div>
                </div>
                <div class="col-sm -2">
                    <input class="btn btn-primary p-t6 loading" type="submit" value="SAVE" >
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>

@endsection
