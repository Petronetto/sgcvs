@extends('main')

@section('title', 'Vessel | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Records
            <span class="font-l">/ Vessel</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('vessel.store') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-body p-t7">
                <div class="row form-horizontal">
                    <div class="col-sm-5">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 control-label">
                                <span class="red">*</span> Name
                            </label>
                            <div class="col-sm-9">
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}" uppercase>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 p-t4">
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 control-label">
                                <span class="red">*</span> E-mail
                            </label>
                            <div class="col-sm-9">
                                <input name="email" type="email" class="form-control" value="{{ old('email') }}" lowercase>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary btn-block loading" type="submit" value="SAVE">
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="container M4">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Name</th>
                        <th>E-mail</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vessels as $vessel)
                    <tr>
                        <td>{{ $vessel->name }}</td>
                        <td>{{ $vessel->email }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('vessel.edit', $vessel->id) }}" class="btn btn-default iframe fancybox.iframe border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('vessel.destroy', $vessel->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-delete=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete the vessel {{ $vessel->name }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('footer')

@endsection
