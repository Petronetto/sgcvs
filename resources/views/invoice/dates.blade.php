@extends('main')

@section('title', 'Edit Dates | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('content')

    <div class="container p-t1">
        <h3>
            <span class="font-l">Vessel:</span> {{ $invoice->vessel->name }}
            <span class="font-l">/ Invoice:</span> {{ $invoice->number }}
        </h3>
        <form action="{{ route('invoice.datesUpdate', $invoice->id) }}" method="post">
            {!! csrf_field() !!}
            <input name="_method" type="hidden" value="PUT">
            <input name="form" type="hidden" value="dates">
            @include('messages')
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row form-horizontal">
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="eta" class="col-sm-3 control-label">ETA</label>
                                <div class="col-sm-9">
                                    <input name="eta" type="text" class="form-control date" value="{{ $invoice->date->eta }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 p-t4">
                            <div class="form-group row">
                                <label for="etb" class="col-sm-3 control-label">ETB</label>
                                <div class="col-sm-9">
                                    <input name="etb" type="text" class="form-control date" value="{{ $invoice->date->etb or '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 p-t4">
                            <div class="form-group row">
                                <label for="ets" class="col-sm-3 control-label">ETS</label>
                                <div class="col-sm-9">
                                    <input name="ets" type="text" class="form-control date" value="{{ $invoice->date->ets or '' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer clearfix p-t1">
                    <div class="col-sm-12 p-t4 text-center">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="join_request" value="1" {{ $invoice->remark->join_request ? 'checked':'' }}> Solicitação de ingresso
                        </label>
                        <div class="p-t1"></div>
                        <textarea name="obs" id="info" cols="30" rows="3" class="form-control" placeholder="Observations">{{ $invoice->obs }}</textarea>
                        <div class="p-t1"></div>
                        <div class="col-sm-2">
                            <input class="btn btn-primary loading" type="submit" value="UPDATE" >
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>

@endsection
