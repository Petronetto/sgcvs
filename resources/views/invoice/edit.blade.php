@extends('main')

@section('title', 'Edit Invoice | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
            <span class="font-l">/ Line-up</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container m-01" id="app">
    <form action="{{ route('invoice.update', $invoice->id) }}" method="post">
        {!! csrf_field() !!}
        <input name="_method" type="hidden" value="PUT">
        @include('messages')
        @if(in_array(Auth::user()->group->id, [1, 2]))
            @include('invoice.edit.commercial')
        @else
            @include('invoice.edit.operational')
        @endif
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Operational Updates</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="supply" class="col-sm-3 control-label">Supply Date</label>
                            <div class="col-sm-9">
                                <input name="supply" v-model="supply" type="text" class="form-control date" value="{{ $invoice->date->supply or '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="estimated" class="col-sm-3 control-label">Estimated Date</label>
                            <div class="col-sm-9">
                                <input name="estimated" v-model="estimated" type="text" class="form-control date" value="{{ $invoice->date->estimated or '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hour" class="col-sm-3 control-label">Hour</label>
                            <div class="col-sm-9">
                                <input name="hour" type="time" class="form-control" value="{{ $invoice->date->hour }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nfe" class="col-sm-3 control-label">NFe</label>
                            <div class="col-sm-9">
                                <input name="nfe" type="text" class="form-control" value="{{ $invoice->nfe }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <spam class="col-sm-3"></spam>
                            <div class="col-md-9">
                                <label class="checkbox-inline">
                                    <input name="confirmation" id="confirmation" type="checkbox" value="1"> Send E-mail Confirmation
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-t4">
                        <div class="form-group row">
                            <label for="vehicle_id" class="col-sm-3 control-label">Vehicle</label>
                            <div class="col-sm-9">
                                <select name="vehicle_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach($vehicles as $vehicle)
                                        <option value="{{ $vehicle->id }}" {{ $vehicle->id == $invoice->vehicle_id ? 'selected':'' }}>{{ $vehicle->plaque }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="employer1_id" class="col-sm-3 control-label">Emp. 1</label>
                            <div class="col-sm-9">
                                <select name="employer1_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach($employers as $employer)
                                        <option value="{{ $employer->id }}" {{ $employer->id == $invoice->employer1_id ? 'selected':'' }}>{{ $employer->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="employer2_id" class="col-sm-3 control-label">Emp. 2</label>
                            <div class="col-sm-9">
                                <select name="employer2_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach($employers as $employer)
                                        <option value="{{ $employer->id }}" {{ $employer->id == $invoice->employer2_id ? 'selected':'' }}>{{ $employer->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="employer3_id" class="col-sm-3 control-label">Emp. 3</label>
                            <div class="col-sm-9">
                                <select name="employer3_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach($employers as $employer)
                                        <option value="{{ $employer->id }}" {{ $employer->id == $invoice->employer3_id ? 'selected':'' }}>{{ $employer->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            <div class="panel-footer clearfix">
                <div class="col-sm-12  text-center">
                    <strong>IMPORTANTE:</strong><br>
                    Apenas com o preenchimento da <em><u>Supply Date</u></em> que o sistema enviará automaticamente
                    um e-mail de confirmação para o R.D.A, Vessel e Responsável VS, informando a data/hora para
                    fornecimento. Tome cuidado, use a <em><u>Estimated Date</u></em> caso ainda não saiba a
                    <em><u>Supply Date</u></em> correta mas já queira inserir na lista de programação.
                </div>
            </div>
        </div>
        <div class="panel panel-info" v-if="supply || estimated" transition="expand">
            <div class="panel-heading">
                <h3 class="panel-title">Conclusion</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-10">
                        <div class="form-group row">
                            <label for="status_id" class="col-sm-3 control-label">Did the ship get supplied?</label>
                            <div class="col-sm-9">
                                <select name="status_id" class="form-control">
                                    @foreach($status as $s)
                                        <option value="{{ $s->id }}" {{ $s->id == $invoice->status_id ? 'selected':'' }}>{{ $s->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            <div class="panel-footer clearfix">
                <div class="col-sm-12  text-center">
                    <strong>ATENÇÃO:</strong><br> Automaticamente, ao salvar este status, ele sairá da sua lista de line-up.
                </div>
            </div>
        </div>
    </form>
    </div>

@include('footer')

@endsection
