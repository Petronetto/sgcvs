<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Informations</h3>
    </div>
    <div class="panel-body">
        <div class="row form-horizontal p-t4">
            <div class="col-sm-6">
                <div class="form-group row">
                    <label for="invoice" class="col-sm-3 control-label"> Invoice</label>
                    <div class="col-sm-9">
                        <input type="text" name="number" class="form-control" value="{{ $invoice->number }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="grupos" class="col-sm-3 control-label"> Groups</label>
                    <div class="col-sm-9">
                        <input type="text" name="groups" class="form-control" value="{{ $invoice->groups }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="vessel_id" class="col-sm-3 control-label">Vessel</label>
                    <div class="col-sm-9">
                        <select name="vessel_id" class="form-control">
                            <option disabled>Select...</option>
                            @foreach($vessels as $vessel)
                                <option value="{{ $vessel->id }}" {{ $vessel->id == $invoice->vessel_id ? 'selected':'' }}>{{ $vessel->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="port_id" class="col-sm-3 control-label">Port</label>
                    <div class="col-sm-9">
                        <select name="port_id" id="port" class="form-control">
                            <option disabled>Select...</option>
                            @foreach($ports as $port)
                                <option value="{{ $port->id }}" {{ $port->id == $invoice->port_id ? 'selected':'' }}>{{ $port->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="agency_id" class="col-sm-3 control-label">Agency</label>
                    <div class="col-sm-9">
                        <select name="agency_id" class="form-control">
                            <option disabled>Select...</option>
                            @foreach($agencies as $agency)
                                <option value="{{ $agency->id }}" {{ $agency->id  == $invoice->agency_id ? 'selected':'' }}>{{ $agency->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- Se o porto tiver está condição, mostrar este campo. -->
                 <div class="form-group row">
                    <label for="boat_cost" class="col-sm-3 control-label">Boat Service</label>
                    <div class="col-sm-9">
                        <input name="boat_cost" id="boat_cost" type="text" class="form-control money" value="{{ $invoice->value->boat_cost }}">
                    </div>
                </div>
                <!-- Se o porto tiver está condição, mostrar este campo. -->
                 <div class="form-group row">
                    <label for="stevedore_cost" class="col-sm-3 control-label">Stevedore</label>
                    <div class="col-sm-9">
                        <input name='stevedore_cost' id="stevedore_cost" type="text" class="form-control money" value="{{ $invoice->value->stevedore_cost }}">
                    </div>
                </div>
                 <div class="form-group row">
                    <label for="transport" class="col-sm-3 control-label">Transportation</label>
                    <div class="col-sm-9">
                        <input name="transport" id="transport" type="text" class="form-control money" value="{{ $invoice->value->transport }}">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                 <div class="form-group row">
                    <label for="discount_1" class="col-sm-3 control-label">Disc. 1</label>
                    <div class="col-sm-9">
                        <select name="discount_1" id="discount_1" class="form-control">
                            <option value="1">No discount</option>
                            @for($i=5; $i <= 30; $i += 5)
                                    <option value="{{ (1-($i/100)) }}">{{ $i }}%</option>
                            @endfor
                            <option disabled>-----------------------</option>
                            @for($i=1; $i <= 30; $i++)
                                <option value="{{ (1-($i/100)) }}" {{ $invoice->value->percent_1 == $i ? 'selected':'' }}>{{ $i }}%</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="discount_2" class="col-sm-3 control-label">Disc. 2</label>
                    <div class="col-sm-9">
                        <select name="discount_2" id="discount_2" class="form-control">
                            <option value="1">No discount</option>
                            @for($i=5; $i <= 30; $i += 5)
                                    <option value="{{ (1-($i/100)) }}">{{ $i }}%</option>
                            @endfor
                            <option disabled>-----------------------</option>
                            @for($i=1; $i <= 30; $i++)
                                <option value="{{ (1-($i/100)) }}" {{ $invoice->value->percent_2 == $i ? 'selected':'' }}>{{ $i }}%</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="eta" class="col-sm-3 control-label">ETA</label>
                    <div class="col-sm-9">
                        <input name="eta" type="text" class="form-control date" value="{{ $invoice->date->eta }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="etb" class="col-sm-3 control-label">ETB</label>
                    <div class="col-sm-9">
                        <input name="etb" type="text" class="form-control date" value="{{ $invoice->date->etb or '' }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ets" class="col-sm-3 control-label">ETS</label>
                    <div class="col-sm-9">
                        <input name="ets" type="text" class="form-control date" value="{{ $invoice->date->ets or '' }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="rda_id" class="col-sm-3 control-label">R.D.A</label>
                    <div class="col-sm-9">
                        <select name="rda_id" id="rda" class="form-control">
                            <option disabled>Select...</option>
                            @foreach($rdas as $rda)
                                <option value="{{ $rda->id }}" {{ $rda->id  == $invoice->rda_id ? 'selected':'' }}>{{ $rda->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="corda_id" class="col-sm-3 control-label">CO-R.D.A</label>
                    <div class="col-sm-9">
                        <select name="corda_id" id="corda_id" class="form-control">
                            <option value="">Select...</option>
                            @foreach($rdas as $rda)
                                <option value="{{ $rda->id }}" {{ $rda->id  == $invoice->corda_id ? 'selected':'' }}>{{ $rda->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer clearfix p-t1">
        <div class="col-sm-12 p-t4 text-center">
            <label class="checkbox-inline">
                <input type="checkbox" name="nautical_chart" value="1" {{ $invoice->remark->nautical_chart ? 'checked':'' }}> Carta Náutica
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="join_request" value="1" {{ $invoice->remark->join_request ? 'checked':'' }}> Solicitação de ingresso
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="feedback" value="1" {{ $invoice->remark->feedback ? 'checked':'' }}> Feedback Form/Invoice
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="medicines" value="1" {{ $invoice->remark->medicines ? 'checked':'' }}> Medicamentos
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="boat_vs" value="1" {{ $invoice->remark->boat_vs ? 'checked':'' }}> Lancha via VS
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="stowage_vs" value="1" {{ $invoice->remark->stowage_vs ? 'checked':'' }}> Estiva via VS
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="boat_client" value="1" {{ $invoice->remark->boat_client ? 'checked':'' }}> Lancha via agência
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="stowage_client" value="1" {{ $invoice->remark->stowage_client ? 'checked':'' }}> Estiva via agência
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="other_remark" v-model="other" {{ $invoice->remark->other ? 'checked':'' }}> Outros
            </label>
            <div class="p-t1" v-if="other" transition="expand">
                <textarea name="other" cols="30" rows="1" class="form-control" style="resize:none" placeholder="Tip your remark">{{ $invoice->remark->other }}</textarea>
            </div>
            <div class="p-t1"></div>
            <textarea name="obs" id="info" cols="30" rows="3" class="form-control" placeholder="Observations">{{ $invoice->obs }}</textarea>
            <div class="p-t1"></div>
        </div>
    </div>
</div>
