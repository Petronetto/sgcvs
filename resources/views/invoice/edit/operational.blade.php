<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Informations</h3>
    </div>
    <div class="panel-body">
        <div class="row form-horizontal p-t4">
            <div class="col-sm-6">
                <div class="form-group row">
                    <label for="invoice" class="col-sm-3 control-label"> Invoice</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ $invoice->number }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="grupos" class="col-sm-3 control-label"> Groups</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ $invoice->groups }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="vessel" class="col-sm-3 control-label">Vessel</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ $invoice->vessel->name }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="port_id" class="col-sm-3 control-label">Port</label>
                    <div class="col-sm-6" v-if="!edit_port">
                        <p class="form-control-static">{{ $invoice->port->name }}</p>
                    </div>
                    <div class="col-sm-6" v-if="edit_port">
                        <select name="port_id" id="port" class="form-control">
                            <option disabled>Select...</option>
                            @foreach($ports as $port)
                                <option value="{{ $port->id }}" {{ $port->id == $invoice->port_id ? 'selected':'' }}>{{ $port->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="edit_port" v-model="edit_port" > Edit
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="agency_id" class="col-sm-3 control-label">Agency</label>
                    <div class="col-sm-6" v-if="!edit_agency">
                        <p class="form-control-static">{{ $invoice->agency->name }}</p>
                    </div>
                    <div class="col-sm-6" v-if="edit_agency">
                        <select name="agency_id" class="form-control">
                            <option disabled>Select...</option>
                            @foreach($agencies as $agency)
                                <option value="{{ $agency->id }}" {{ $agency->id  == $invoice->agency_id ? 'selected':'' }}>{{ $agency->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="edit_agency" v-model="edit_agency" > Edit
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="corda" class="col-sm-3 control-label">Boat Service</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">$ {{ $invoice->value->boat_cost }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="corda" class="col-sm-3 control-label">Stevedore</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">$ {{ $invoice->value->stevedore_cost }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="corda" class="col-sm-3 control-label">Transportation</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">$ {{ $invoice->value->transport }}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                    <label for="corda" class="col-sm-3 control-label">Disc. 1</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ 100-($invoice->value->discount_1 * 100) }}%</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="corda" class="col-sm-3 control-label">Disc. 2</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ 100-($invoice->value->discount_2 * 100) }}%</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="eta" class="col-sm-3 control-label"><span class="red">*</span> ETA</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ $invoice->date->eta }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="etb" class="col-sm-3 control-label">ETB</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ $invoice->date->etb }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ets" class="col-sm-3 control-label">ETS</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ $invoice->date->ets }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="rda" class="col-sm-3 control-label">R.D.A</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ $invoice->rda->name }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="corda" class="col-sm-3 control-label">CO-R.D.A</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">{{ @$invoice->corda->name }}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lista-status" class="col-sm-3 control-label">Remarks</label>
                    <div class="col-sm-9">
                        <ul class="lista-status">
                            @if($invoice->remark->nautical_chart) <li>Carta Náutica</li> @endif
                            @if($invoice->remark->join_request) <li>Solicitação de ingresso</li> @endif
                            @if($invoice->remark->feedback) <li>Feedback Form/Invoice</li> @endif
                            @if($invoice->remark->medicines) <li>Medicamentos</li> @endif
                            @if($invoice->remark->boat_vs) <li>Lancha via VS</li> @endif
                            @if($invoice->remark->stowage_vs) <li>Estiva via VS</li> @endif
                            @if($invoice->remark->boat_client) <li>Lancha via agência</li> @endif
                            @if($invoice->remark->stowage_client) <li>Estiva via agência</li> @endif
                            @if($invoice->remark->other) <li>{{ $invoice->remark->other }}</li> @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group row">
                    <label for="" class="col-sm-2 control-label"> Observations</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{ $invoice->obs }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
