<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("groups")->insert([
            [
                "name" => "Commercial Management"
            ],
            [
                "name" => "Commercial"
            ],
            [
                "name" => "Operational Management"
            ],
            [
                "name" => "Operational"
            ]
        ]);

        $this->command->info('Table groups seeded!');
    }
}
