<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("status")->insert([
            [
                'id'    => 1,
                'name'  => 'Not Supplied'
            ],
            [
                'id'    => 2,
                'name'  => 'Supplied'
            ],
            [
                'id'    => 3,
                'name'  => 'Cancelled'
            ]
        ]);

        $this->command->info('Table Status seeded!');
    }
}
