<?php

use Illuminate\Database\Seeder;

class ConclusionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("conclusions")->insert([
            [
                'id'        => 1,
                'name'  => 'NO'
            ],
            [
                'id'        => 2,
                'name'  => 'YES'
            ],
            [
                'id'        => 3,
                'name'  => 'YES WITH +'
            ],
            [
                'id'        => 4,
                'name'  => 'YES WITH -'
            ],
            [
                'id'        => 5,
                'name'  => 'DEFRAUDED'
            ],
        ]);

        $this->command->info('Table Settles seeded!');
    }
}
