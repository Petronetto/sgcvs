<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
            DB::table("invoices")->insert([
                [
                    'user_id'         => $faker->numberBetween($min = 1, $max = 4),
                    'locale_id'       => rand(1,2),
                    'vessel_id'       => $faker->numberBetween($min = 1, $max = 10),
                    'rda_id'          => $faker->numberBetween($min = 1, $max = 10),
                    'corda_id'        => $faker->optional()->numberBetween($min = 1, $max = 10),
                    'agency_id'       => $faker->numberBetween($min = 1, $max = 10),
                    'port_id'         => $faker->numberBetween($min = 1, $max = 10),
                    'employer1_id'    => $faker->numberBetween($min = 1, $max = 10),
                    'employer2_id'    => $faker->optional()->numberBetween($min = 1, $max = 10),
                    'employer3_id'    => $faker->optional()->numberBetween($min = 1, $max = 10),
                    'vehicle_id'      => $faker->numberBetween($min = 1, $max = 10),
                    'status_id'       => rand(1,3),
                    'conclusion_id'   => rand(1,5),
                    'stage'           => rand(1,5),
                    'number'          => '00000'.$index,
                    'nfe'             => 'NFe-00000'.$index,
                    'groups'          => ucfirst($faker->word),
                    'lawyer'          => $faker->boolean,
                    'under_process'   => $faker->boolean,
                    'obs'             => $faker->sentence
                ]
            ]);
        }

        foreach (range(1,10) as $index) {
            DB::table("values")->insert([
                [
                    'invoice_id'          => $index,
                    'gross_1'             => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 5000),
                    'discount_1'          => $faker->randomFloat($nbMaxDecimals = 2, $min = 0.7, $max = 1),
                    'gross_2'             => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 5000),
                    'discount_2'          => $faker->randomFloat($nbMaxDecimals = 2, $min = 0.7, $max = 1),
                    'wo'                  => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'received'            => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 5000),
                    'reference'           => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 5000),
                    'boat_cost'           => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'stevedore_cost'      => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'boat_cost_real'      => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'stevedore_cost_real' => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'transport'           => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'purch_value_nfe'     => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'freight'             => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 500),
                    'payment_remarks'     => $faker->sentence,
                ]
            ]);
        }

        foreach (range(1,10) as $index) {
            $values = rand(1, 20);
            DB::table("remarks")->insert([
                [
                    'invoice_id'      => $index,
                    'nautical_chart'  => $values >= $index ? 0 : $faker->boolean,
                    'join_request'    => $values >= $index ? 0 : $faker->boolean,
                    'feedback'        => $values >= $index ? 0 : $faker->boolean,
                    'medicines'       => $values >= $index ? 0 : $faker->boolean,
                    'boat_vs'         => $values >= $index ? 0 : $faker->boolean,
                    'stowage_vs'      => $values >= $index ? 0 : $faker->boolean,
                    'boat_client'     => $values >= $index ? 0 : $faker->boolean,
                    'stowage_client'  => $values >= $index ? 0 : $faker->boolean,
                    'other'           => $values >= $index ? "" : $faker->sentence
                ]
            ]);
        }

        foreach (range(1,10) as $index) {
            DB::table("dates")->insert([
                [
                    'invoice_id'      => $index,
                    'eta'             => $faker->dateTime($max = '2016-06-01 08:00:00'),
                    'etb'             => $faker->dateTime($max = '2016-06-01 08:00:00'),
                    'ets'             => $faker->dateTime($max = '2016-06-01 08:00:00'),
                    'supply'          => $faker->optional()->dateTime($max = '2016-06-01 08:00:00'),
                    'estimated'       => $faker->optional()->dateTime($max = '2016-06-01 08:00:00'),
                    'payment_date'    => $faker->dateTime($max = '2016-06-01 08:00:00'),
                    'first_charge'    => $faker->dateTime($max = '2016-06-01 08:00:00'),
                    'second_charge'   => $faker->optional()->dateTime($max = '2016-06-01 08:00:00'),
                    'third_charge'    => $faker->optional()->dateTime($max = '2016-06-01 08:00:00'),
                    'payment'         => $faker->numberBetween($min = 15, $max = 90),
                    'hour'            => date('h:i',strtotime("+".rand()." hours")),
                ]
            ]);
        }

        $this->command->info('Table invoices seeded!');
    }
}
