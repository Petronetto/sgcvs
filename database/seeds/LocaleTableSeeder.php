<?php

use Illuminate\Database\Seeder;

class LocaleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("locales")->insert([
            [
                "name" => "Vitória"
            ],
            [
                "name" => "Rio de Janeiro"
            ]
        ]);

        $this->command->info('Table locales seeded!');
    }
}
