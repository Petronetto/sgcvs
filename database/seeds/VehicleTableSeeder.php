<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class VehicleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
            DB::table("vehicles")->insert([
                [
                    'plaque' => 'XYZ-'.$faker->numberBetween($min = 1111, $max = 9999),
                    'model'  => strtoupper($faker->word)
                ]
            ]);
        }

        $this->command->info('Table vehicles seeded!');
    }
}
