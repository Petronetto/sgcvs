<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RdaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
            DB::table("rdas")->insert([
                [
                    'name'  => $faker->name,
                    'email' => 'juliano.petronetto@gmail.com',
                    'type'  => $faker->boolean,
                ]
            ]);
        }

        $this->command->info('Table RDA seeded!');
    }
}
