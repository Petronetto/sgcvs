<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dates', function(Blueprint $table)
		{
			$table->foreign('invoice_id', 'fk_dates_invoice')->references('id')->on('invoices')->onUpdate('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dates', function(Blueprint $table)
		{
			$table->dropForeign('fk_dates_invoice');
		});
	}

}
