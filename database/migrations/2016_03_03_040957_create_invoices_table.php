<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->unsignedInteger('locale_id');
			$table->unsignedInteger('vessel_id');
			$table->unsignedInteger('rda_id');
			$table->unsignedInteger('corda_id')->nullable();
			$table->unsignedInteger('agency_id');
			$table->unsignedInteger('port_id');
			$table->unsignedInteger('employer1_id')->nullable();
			$table->unsignedInteger('employer2_id')->nullable();
			$table->unsignedInteger('employer3_id')->nullable();
			$table->unsignedInteger('vehicle_id')->nullable();
			$table->unsignedInteger('conclusion_id')->default(1);
			$table->unsignedInteger('status_id')->default(1);
			$table->integer('stage')->default(1)->comment('1: Line-up; 2: Approval Line-up; 3: Billing; 4: Approval Billing; 5: Historic;');
			$table->string('number', 45);
			$table->string('nfe', 255)->nullable();
			$table->string('groups', 255)->nullable();
			$table->boolean('lawyer')->default(0);
			$table->boolean('under_process')->default(0);
			$table->longText('obs')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
