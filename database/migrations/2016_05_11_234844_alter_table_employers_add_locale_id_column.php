<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmployersAddLocaleIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function (Blueprint $table) {
            $table->unsignedInteger('locale_id')->before('created_at');
            // $table->foreign('locale_id', 'fk_employer_locale')
            //       ->references('id')
            //       ->on('locales')
            //       ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employers', function (Blueprint $table) {
            $table->dropColumn('locale_id');
            //$table->dropForeign('fk_employer_locale');
        });
    }
}
