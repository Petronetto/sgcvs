<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->foreign('agency_id', 'fk_agency')->references('id')->on('agencies')->onUpdate('CASCADE');
			$table->foreign('rda_id', 'fk_rda_id')->references('id')->on('rdas')->onUpdate('CASCADE');
			$table->foreign('corda_id', 'fk_corda_id')->references('id')->on('rdas')->onUpdate('CASCADE');
			$table->foreign('employer1_id', 'fk_employer1_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('employer2_id', 'fk_employer2_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('employer3_id', 'fk_employer3_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('port_id', 'fk_port_id')->references('id')->on('ports')->onUpdate('CASCADE');
			$table->foreign('user_id', 'fk_user_id')->references('id')->on('users')->onUpdate('CASCADE');
			$table->foreign('locale_id', 'fk_locale_user')->references('id')->on('locales')->onUpdate('CASCADE');
			$table->foreign('vehicle_id', 'fk_vehicle_id')->references('id')->on('vehicles')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('vessel_id', 'fk_vessel_id')->references('id')->on('vessels')->onUpdate('CASCADE');
			$table->foreign('conclusion_id', 'fk_conclusion_id')->references('id')->on('conclusions')->onUpdate('CASCADE');
			$table->foreign('status_id', 'fk_status_id')->references('id')->on('status')->onUpdate('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->dropForeign('fk_agency');
			$table->dropForeign('fk_corda_id');
			$table->dropForeign('fk_employer1_id');
			$table->dropForeign('fk_employer2_id');
			$table->dropForeign('fk_employer3_id');
			$table->dropForeign('fk_port_id');
			$table->dropForeign('fk_rda_id');
			$table->dropForeign('fk_user_id');
			$table->dropForeign('fk_locale_user');
			$table->dropForeign('fk_vehicle_id');
			$table->dropForeign('fk_vessel_id');
			$table->dropForeign('fk_conclusion_id');
			$table->dropForeign('fk_status_id');
		});
	}

}
