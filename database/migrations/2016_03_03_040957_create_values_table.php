<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('values', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('invoice_id');
			$table->float('gross_1', 10, 2)->default(0);
			$table->float('discount_1', 3, 2)->default(1);
			$table->float('gross_2', 10, 2)->default(0);
			$table->float('discount_2', 3, 2)->default(1);
			$table->float('wo', 10, 2)->default(0);;
			$table->float('received', 10, 2)->default(0);
			$table->float('reference', 10, 2)->default(0);
			$table->text('payment_remarks', 65535)->nullable();
			$table->float('boat_cost', 10, 2)->default(0);
			$table->float('stevedore_cost', 10, 2)->default(0);
			$table->float('transport', 10, 2)->default(0);
			$table->float('boat_cost_real', 10, 2)->default(0);
			$table->float('stevedore_cost_real', 10, 2)->default(0);
			$table->float('purch_value_nfe', 10, 2)->default(0);
			$table->float('freight', 10, 2)->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('values');
	}

}
