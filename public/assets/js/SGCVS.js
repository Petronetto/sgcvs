// Show de Loading image in transitions
$(window).load(function() {
    // Call loading
    $('body').on('click', '.loading', function(e) {
        $(".loader").fadeIn("fast");
    });
    // Animate loader off screen
    $(".loader").fadeOut("fast");
    setTimeout(function() {
        $(".loader").fadeOut("fast")
    }, 8000);
});

$(document).ready(function() {

    // Return Port stevedore and boat information (on load)
    $(window).on('load', function() {
        var id = $('#port option:selected').val();
        if (!id) {
            $('#boat_cost').prop('disabled',true);
            $('#stevedore_cost').prop('disabled',true);

            return;
        }
        $.ajax({
            type: "GET",
            url: '/port/data/'+id,
        }).done(function(port){
            if(!port.boat) {
                $('#boat_cost').prop('disabled',true);
            } else {
                $('#boat_cost').prop('disabled',false);
            }

            if(!port.stevedore) {
                $('#stevedore_cost').prop('disabled',true);
            } else {
                $('#stevedore_cost').prop('disabled',false);
            }
       }).fail(function() {
           $('#boat_cost').prop('disabled',true);
           $('#stevedore_cost').prop('disabled',true);
       });
    });

    // Return Port stevedore and boat information
    $('body').on('change', '#port', function(e) {
        var id = $('#port option:selected').val();
        $.ajax({
            type: "GET",
            url: '/port/data/'+id,
        }).done(function(port){
            if(!port.boat) {
                $('#boat_cost').prop('disabled',true);
            } else {
                $('#boat_cost').prop('disabled',false);
            }

            if(!port.stevedore) {
                $('#stevedore_cost').prop('disabled',true);
            } else {
                $('#stevedore_cost').prop('disabled',false);
            }
       });
    });

    // E-mail confirmation checkbox
    $('body').on('change', 'input[name=supply]', function(e) {
        if($('input[name=supply]').val()) {
            $('#confirmation').attr('checked', true);
        }
    });


    // DataTabels instance
    $.fn.dataTable.moment( 'D/M/YYYY' );
    $('.data-table').DataTable({
         stateSave: true,
         paging: false,
         dom: 'Bfrtip',
         language: {
             emptyTable: "No results"
         },
         buttons: [
             'copy',
             'excel', {
                 extend: 'pdfHtml5',
                 orientation: 'landscape',
                 pageSize: 'LEGAL'
             },
             'print',
             'colvis'
         ]
    });

    // Table for Reports to calculate the values on bottom
    $('#report').DataTable({
        stateSave: true,
        paging: false,
        dom: 'Bfrtip',
        language: {
            emptyTable: "No results"
        },
        buttons: [
            'copy',
            'excel', {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            'print',
            'colvis'
        ],
        footerCallback: function ( row, data, start, end, display ) {
            var usd = this.api(), data;
            var brl = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages US$
            total = usd
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page US$
            pageTotal = usd
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer US$
            $( usd.column( 11 ).footer() ).html(
                pageTotal.toFixed(2)
            );


            // Total over all pages R$
            total = brl
                .column( 14 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page R$
            pageTotal = brl
                .column( 14, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer R$
            $( brl.column( 14 ).footer() ).html(
                pageTotal.toFixed(2)
            );
        }
    });

    $( ".date" ).datepicker({
    	dateFormat: "dd/mm/yy",
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( ".date" ).datepicker( "option", selectedDate );
        }
    });

    // Iframe
    $(".iframe").fancybox({
        closeBtn : true,
        padding : 0,
        tpl: {
             prev : '<a class="f-direita" href="javascript:;"><span></span></a>',
             next : '<a class="f-esquerda" href="javascript:;"><span></span></a>',
        },
        iframe : {
            scrolling : 'no'
        },
        maxWidth : 700,
        maxHeight : 400,
        mouseWheel : false,
        scrollOutside: true,
        fitToView : false,
        width : '100%',
        height : '100%',
        autoSize : false,
        closeClick : false,
        openEffect : 'fade',
        closeEffect : 'fade',
        helpers : {
            keyboard : null,
            overlay : {
                opacity : 0.8, // or the opacity you want
                css : { 'background': 'rgba(0, 0, 0, 0.8)' }// or your preferred hex color value
            } // overlay
        }, // helpers
        afterClose: function () { // reload de page to update results
            parent.location.reload(true);
        }
    });

    // Iframe
    $(".iframe2").fancybox({
        closeBtn : true,
        padding : 0,
        tpl: {
             prev : '<a class="f-direita" href="javascript:;"><span></span></a>',
             next : '<a class="f-esquerda" href="javascript:;"><span></span></a>',
        },
        iframe : {
            scrolling : 'no'
        },
        maxWidth : 700,
        maxHeight : 600,
        mouseWheel : false,
        scrollOutside: true,
        fitToView : false,
        width : '100%',
        height : '100%',
        autoSize : false,
        closeClick : false,
        openEffect : 'fade',
        closeEffect : 'fade',
        helpers : {
            keyboard : null,
            overlay : {
                opacity : 0.8, // or the opacity you want
                css : { 'background': 'rgba(0, 0, 0, 0.8)' }// or your preferred hex color value
            } // overlay
        }, // helpers
        afterClose: function () { // reload de page to update results
            parent.location.reload(true);
        }
    });

    // Máscaras
    $('.cpf').mask("999.999.999-99");
    $('.cell').mask('(99) 99999-9999');
    $('.phone').mask('(99) 9999-9999');
    $('.plaque').mask("AAA-9999");
    $('.date').mask("00/00/0000");
    //$('.number').mask('ZZZ', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});

    $('.money').maskMoney({thousands:'', decimal:'.'});

    $('body').on('keyup', 'input[type=text]', function() {
        this.value = this.value.toUpperCase();
    });

});
